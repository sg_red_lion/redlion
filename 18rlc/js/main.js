/**
 * Main Javascript.
 * This file is for who want to make this theme as a new parent theme and you are ready to code your js here.
 */

$('.rlc-read-more').click(function () {

    var str = $(this).children().html();
    if (str.includes('more') === true) {
        $(this).children().html('See less <i class="caret-top"></i>');
    } else {
        $(this).children().html('See more <i class="caret"></i>');
    }

});

