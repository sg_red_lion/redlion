<?php
/**
 * The theme footer
 * 
 * @package bootstrap-basic
 */
?>
<script>


function mobileMenuReorder(){
	var windowWidth = $( window ).width(),
	    thirdLevel = $('.mega-menu-item .mega-menu-item-has-children > .mega-sub-menu'),
		secondLevelWithChildren = thirdLevel.prev();
		
	if(windowWidth < 767){
		
		thirdLevel.css('padding', '0');
		thirdLevel.removeClass('mega-sub-menu').addClass('collapse mega-sub-menu-removed');
		secondLevelWithChildren.append('<span class="second-level-menu">+</span>').removeAttr("href");
		
		$('.collapse').on('shown.bs.collapse', function (e) {
			$('#'+$(this).prev().attr('id') + ' > .second-level-menu').text('-');
        }).on('hidden.bs.collapse', function (e) {
			$('#'+$(this).prev().attr('id') + ' > .second-level-menu').text('+');
        });

		$(secondLevelWithChildren).each(function( index, element) {
			 $(this).attr('data-toggle', 'collapse').attr('data-target', '#third-level-ul-'+index).attr('id', 'second-level-a-'+index);
			 $(this).next('ul').attr('id', 'third-level-ul-'+index);
		});
	}else{
		 $('.mega-sub-menu-removed').removeClass('collapse').removeClass('mega-sub-menu-removed').addClass('mega-sub-menu');
		 $('.second-level-menu').text('');
		
	}

}
/*
function resizeHomepageNews(){
	var maxHeight = 0;
	$('.news-column').each(function(){
		var thisH = $(this).height();
	    if (thisH > maxHeight) { maxHeight = thisH; }
	});
	$('.news-column .panel-default').height(maxHeight+15);
}*/

mobileMenuReorder();
$(window).resize(mobileMenuReorder);

</script>

</div><!--end #content-wrapper-->
<div class="clearfix"></div>

    <button class="btn" id="backToTop"><?php _e(" Back to Top", 'bootstrap-basic'); ?> </button>
	<footer id="footer" >
		<div class="padding20"></div>
		<div class="col-xs-12 col-md-4">
			<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
				<img src="<?php echo get_template_directory_uri();?>/img/logo-white.png" class="img-responsive footer-logo" alt="logo">
			</a>
		</div>
		
		<div class="col-xs-12 col-md-8 footer-menu text-right">
			<?php 
				$footerMenu = wp_get_nav_menu_items('footer-menu');

				foreach($footerMenu as $key => $footerMenuItem){
					echo '<div class="footer-menu-item text-center-"><a href="'.get_permalink($footerMenuItem->object_id).'">'.$footerMenuItem->title.'</a></div>';
				}			
			?>
		
		</div>
		
		<div class="visible-xs col-xs-12 text-center">
			<a href="<?php echo get_option('general_setting_linkedin'); ?>" target="_blank" class="padding5">
				<img src="<?php echo get_template_directory_uri();?>/img/linked.png" alt="linkedIn">
			</a>
			<a href="<?php echo get_option('general_setting_twitter'); ?>" target="_blank" class="padding5">
				<img src="<?php echo get_template_directory_uri();?>/img/twitter.png" alt="twitter">
			</a>
			<div class="col-xs-12 text-center padding20 cookie"><?php _e("Cookie Policy", 'bootstrap-basic'); ?></div>
			<div class="col-sm-12 text-center copyright">
				<?php _e("Copyright ", 'bootstrap-basic'); ?> &COPY; <?php echo date('Y'); ?><?php _e("Red Lion Chambers.<br/> All rights reserved.", 'bootstrap-basic'); ?>
			</div>
		</div>
		<div class="hidden-xs col-sm-12 text-right copyright">
			<?php _e("Copyright ", 'bootstrap-basic'); ?> &COPY; <?php echo date('Y'); ?> <?php _e("Red Lion Chambers. All rights reserved.", 'bootstrap-basic'); ?>
		</div>
		<div class="clearfix"></div>
		<div class="padding5"></div>
		<div class="hidden-xs col-sm-12">
			
			<div class=" col-sm-2 col-lg-1 pull-right ">
				<a href="<?php echo get_option('general_setting_linkedin'); ?>" target="_blank" class="paddingR5">
					<img src="<?php echo get_template_directory_uri();?>/img/linked.png" alt="linkedIn" class="footer-social-icons">
				</a>
				<a href="<?php echo get_option('general_setting_twitter'); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri();?>/img/twitter.png" alt="twitter" class="footer-social-icons">
				</a>
			</div>
			<div class="col-sm-10 col-lg-11 text-right cookie"><?php _e("Cookie Policy", 'bootstrap-basic'); ?></div>
		</div><div class="clearfix"></div>
	</footer>
<!--
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/flexslider.css" />
	<script src="<?php echo get_template_directory_uri();?>/js/vendor/jquery.flexslider.js"></script>
-->
    <?php  wp_enqueue_script('custom-alert-script', get_template_directory_uri() . '/js/custom-alert.js', array(), false, true); ?>
	<?php wp_footer(); ?> 
</body>
</html>
