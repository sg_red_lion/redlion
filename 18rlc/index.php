<?php
/**
 * The main template file
 * 
 * @package bootstrap-basic
 */

get_header();

/**
 * determine main column size from actived sidebar
 */
	//$main_column_size = bootstrapBasicGetMainColumnSize();
	$news_args = array(
		'posts_per_page' => 4,
		//'category_name' => 'news',
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'post',
		'post_status' => 'publish',
		'meta_query'       => array(
			'relation'    => 'AND',
			//meta field condition one
			'firstpage_clause' =>array(
				'key'          => 'first-page',
				'value'        => 'yes',
			),
		)
	);
	 $news = new WP_Query( $news_args );

	?>
	
	<?php //echo do_shortcode("[metaslider id=22]");?>
	<?php echo do_shortcode('[rev_slider alias="revo-homepage"]');?>
	<div class="clearfix"></div><br/><br/>
	<div class="homepageHowWeAreContainer">
        <div id="homepageHowWeAreImg"></div>
        <div class="homepageHowWeAreContent">
            <div class="col-sm-2"></div>
            <div class="col-xs-12 col-sm-8 padding0">
                <div class="col-xs-3"><hr></div>
                <div class="col-xs-6" id="homepageHowWeAreTitle"> 
                    <?php echo get_option('how_are_we_title_setting'); ?>
                    <div class="padding5 hidden-xs"></div>
                </div>
                <div class="col-xs-3"><hr></div>
            </div>
            <div class="col-sm-2"></div>

            <div class="clearfix"></div>
            <span id="homepageHowWeAreDescription">
                <?php echo get_option('how_are_we_setting'); ?>
            </span>
            <div class="clearfix"></div>
            <div class="col-xs-3 col-sm-2"></div>
            <div class="col-xs-6 col-sm-8 padding0"><hr class="homepageHowWeDevider"></div>
            <div class="col-xs-3 col-sm-2"></div>
            <div class="clearfix"></div>
            <a href="#" id="how-are-we-btn" class="visible-xs">Read More</a>
            <div class="clearfix"></div>
        </div>
    </div>


	<div class="clearfix"></div><br/>
	<div class="container-fluid">
		<div class="col-sm-1 hidden-xs" >
			<div class="redLine"></div>
			<br/>
		</div>
		<div class="clearfix"></div>
	
		<div class="col-md-6 col-lg-4" id="featuresOne">
			<?php echo get_option('setting_features_one');?>
		</div>
		<div class="col-md-6 col-lg-4"  id="featuresTwo">
			<?php echo get_option('setting_features_two');?>
		</div>
		<div class="col-md-6 col-lg-4"  id="featuresThree">
			<?php echo get_option('setting_features_three');?>
		</div>
	</div>
		
		<div class="clearfix"></div><hr class="padding5"/>
		<script type="text/javascript">
			
			$(window).resize(toggleNewsBody);
			$('.panel-body .collapse').addClass('in');
			function changeIcon(collapseBtnId){
				$('#'+collapseBtnId).text(function(i,defaultIcon){
					return defaultIcon =='+' ?  '-' : '+';
				});
			}
			function toggleNewsBody(){

				if(window.innerWidth < 767){	
					$('.news-container').removeClass('in');
					$('.expand-news').slideDown();
				}else{		
					$('.news-container').addClass('in');
					$('.expand-news').slideUp();
				}
			}
			$( document ).ready(function() {
					toggleNewsBody();
			});
		
		</script>

	<?php 
	while ( $news->have_posts() ) : $news->the_post();?>
		<div class="col-sm-6 col-md-3 news-column">
			<div class="panel panel-default ">
			  <div class="panel-heading padding0">
				    <div class="news-img">
						<img src="<?php echo get_the_post_thumbnail_url() ?>" class="img-responsive"  alt="<?php echo get_the_title() ?>">
						<div class="news-title">
							<?php echo get_the_title();?>
						</div>
						<span class="expand-news" id="collapse-btn-<?php the_ID();?>" onclick="changeIcon('collapse-btn-<?php the_ID();?>')" data-toggle="collapse" data-target="#news-<?php the_ID();?>">+</span>
					</div>
			  </div>
			  <div class="news-container panel-body collapse" id="news-<?php the_ID();?>">
				  <div class="news-date">
						<?php echo get_the_date("j. M Y"); ?>
				  </div>
				  <div class="news-date-devider"></div>
				  <div class="clearfix"></div><br>
				  <div class="news-excerpt">
						<?php the_excerpt(); ?> 
						<div class="clearfix"></div>
						<a href="<?php echo get_permalink();?>" class="btn btn-read-more marginT20">View</a>
				  </div> 
				  
			  </div>
			</div>
		
			
		</div>
		<?php if (($news->current_post +1) == ($news->post_count)) { ?>
			<div class="col-sm-12">
				<a href="<?php echo home_url('/category/news'); ?>" class="btn btn-view-all-news">View All News</a>	
			</div>
		<?php } ?>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
			
	<div class="clearfix"></div><br/>
	<div class="container-middle">
		<?php echo do_shortcode("[metaslider id=24]"); ?>
	</div>
	<div class="clearfix marginB15"></div>


    <?php 
        $articlesMenu = wp_get_nav_menu_items('articles-menu');
        foreach($articlesMenu as $articleItem){
    ?>
    <div class="col-lg-6 marginTB10 padding0-xs">
        <div class="article-container">
            <div class="col-xs-6 col-sm-8 padding0">
            <?php if(get_field('redirect_to',$articleItem->object_id)){ ?>
                    <a href="<?php echo get_field('redirect_to',$articleItem->object_id); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url($articleItem->object_id); ?>" class="img-responsive" alt="<?php echo $articleItem->title?>"> 
                    </a>
                <?php }else{ ?>
                    <a href="<?php echo get_permalink($articleItem->object_id); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url($articleItem->object_id); ?>" class="img-responsive" alt="<?php echo $articleItem->title?>"> 
                    </a>
                <?php }?>

            </div>
            <div class="col-xs-6 col-sm-4 padding0">
                <div class="article-title">
                <?php if(get_field('redirect_to',$articleItem->object_id)){ ?>
                    <a href="<?php echo get_field('redirect_to',$articleItem->object_id); ?>"><?php echo $articleItem->title?></a>
                <?php }else{ ?>
                    <a href="<?php echo get_permalink($articleItem->object_id); ?>"><?php echo $articleItem->title?></a>
                <?php }?>
                </div>
                <div class="article-devider hidden-xs"></div><br>
                <div class="article-description hidden-xs">
                    <?php  
                        $the_post_content = get_post($articleItem->object_id);
                        echo apply_filters('the_content', $the_post_content->post_content);

                    ?>
                </div>
            </div><div class="clearfix"></div>
        </div>
    </div>
    <?php } ?>

	<div class="clearfix marginB15"></div>

	<div class="flexslider-middle">
	
		<div class="col-sm-12 get-in-touch-container">
		  <div class="backgroundBlack">
			  <div class="col-sm-12">
				<?php echo  do_shortcode('[ninja_form id=4]'); ?>
				<div class="padding5-xs"></div>
			  </div>
		  </div>
		</div>
	
	
		<?php //echo  do_shortcode('[contact-form-7 id="35" title="Get in Touch"]');  ?>
	</div>
	
	<div class="padding5"></div>
	<?php /* ?>
	<div id="map" style="width:100%;height:350px" class="hidden-xs"></div>

   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA07IdV-neqeWKo3aQuds9fnIA2A9ntF5E&callback=initMap" async defer></script>
	<script type="text/javascript">
		var map;
		  function initMap() {
			  var myLatLng = { lat: 51.514856, lng: -0.108934 };
			map = new google.maps.Map(document.getElementById('map'), {
			  center: myLatLng,
			  zoom: 16,
			  scrollwheel: false,
			}); 

			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map
			});

				marker.setMap(map);
		  }
  </script>
	<?php */?>
	<div class="clearfix"></div><br>
	<div class="padding20"></div>
	<div class="container" id="GetInTouch">
		<div class="col-xs-6 col-md-4">
			<?php echo get_option('setting_get_in_touch_one');?>
		</div>
		<div class="col-xs-6 col-md-4">
			<?php echo get_option('setting_get_in_touch_two');?>
		</div>
		<div class="col-md-4 hidden-xs">
			<?php echo get_option('setting_get_in_touch_three');?>
			
			<div class="tweetscroll" style="height:200px;overflow-y:auto;">
				<a class="twitter-timeline" data-tweet-limit="1" data-chrome="noheader noborders nofooter" data-width="270" data-height="220" href="<?php echo get_option('general_setting_twitter'); ?>"></a> 
				<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
			</div>

		</div>
		<div class="clearfix"></div><br>
		<div class="padding20 visible-xs"></div>
	</div>


<?php  get_footer(); ?> 