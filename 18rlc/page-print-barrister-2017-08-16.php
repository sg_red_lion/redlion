<?php
/**
 * Template for displaying pages
 * 
 * @package bootstrap-basic
 */


$barristerId = (int) $_GET['id'];
$barristerObj = get_post($barristerId);


/*echo '<pre>';
var_export($barristerObj);
echo '</pre>';
*/

$silk = get_post_meta($barristerId, 'Silk', true);
$call = get_post_meta($barristerId, 'Call', true);
$phone = get_post_meta($barristerId, 'phone', true);
$email = get_post_meta($barristerId, 'email', true);
$certificate_one = get_post_meta($barristerId, 'certificate_one', true);
$certificate_two = get_post_meta($barristerId, 'certificate_two', true);
$honorary_door_tenant = get_post_meta($barristerId, 'honorary_door_tenant', true);
$area_of_experience = str_replace(array('[/expand]','[su_tabs]', '[/su_tab]' ,'[/su_tabs]'), ' ',wpautop( get_post_meta($barristerId, 'area_of_experience', true)));
$area_of_experience = str_replace(array('[expand title="','[su_tab title="'), ' <h3>', $area_of_experience);
$area_of_experience = str_replace('"]', ' </h3>', $area_of_experience);

$right_barrister_sidebar = str_replace(array('[rlc-read-more]','[/rlc-read-more]'), ' ',wpautop( get_post_meta($barristerId, 'right_barrister_sidebar', true)));
$left_barrister_sidebar = str_replace(array('[rlc-read-more]','[/rlc-read-more]'), ' ',wpautop( get_post_meta($barristerId, 'left_barrister_sidebar', true)));

use Dompdf\Dompdf;
if (isset($_GET['pdf'])) {
$dompdf = new Dompdf();
$logo = get_template_directory()."/img/logo-header.png";
$html = '<style>
 
body {
    font-family: "Lato",serif !important;
}
h1,h2,h3,h4 {
	font-family: "Lato",serif !important;
}
</style>';
$html .= '<table style="width:100%">';
	$html .= '<tr>';
		$html .= '<td width="95%" align="left"><img src="'.$logo.'" width="256"/></td>';
	$html .= '</tr>';
$html .='</table>';

$thumb = '';
if(get_the_post_thumbnail($barristerId, 'medium')){
		$thumb =  get_the_post_thumbnail_url($barristerId, 'medium');
		$thumb = str_replace(get_site_url(), ABSPATH, $thumb);
		
}else{
		$thumb = get_template_directory() . '/img/red pic.png';
}

$c = '';					
$s = '';
$p = '';
$e = '';
$h = '';

$c = 'Call:'.$call.'<br/>';
if($silk){
$s = 'Silk:'.$silk.'<br/>';
}
if($phone){
$p = 'Phone:'.$phone.'<br/>';
}
if($email){
$e = 'Email:'.$email.'<br/>';
}
if($honorary_door_tenant == 'Yes'){
$h = 'Honorary Door Tenant<br/>';
}

$cert_one = ' ';
$cert_two = ' ';

if($certificate_one){  
	$cert_one = '<img src="'. wp_get_attachment_image_url(  get_field('certificate_one',$barristerId),  'medium' ) .'" width="80" />';
	$cert_one = str_replace(get_site_url(), ABSPATH, $cert_one);
}
if($certificate_two){
	$cert_two = '<img src="'.wp_get_attachment_image_url(  get_field('certificate_two',$barristerId),  'medium' ) .'" width="80" />';
	$cert_two = str_replace(get_site_url(), ABSPATH, $cert_two);
}

$html .= '<table style="width:100%">';
	$html .= '<tr>';
		$html .= '<td width="100%" align="left">
			<table width="100%">
				<tr>
					<td align="left" width="25%"><img src="' .$thumb.'" width="200" /></td>
					<td width="5%"> </td>
					<td align="left" valign="top" width="30%"><h1 valign="top" style="font-size: 26px;white-space:nowrap; ">'. $barristerObj->post_title. '</h1>
						Call: '.$call.'<br/>						
						'.$s.'
						'.$p.'
						'.$e.'
						'.$h.'
					</td>
					<td width="5%"> </td>
					<td align="right" width="35%" valign="bottom" style="text-align:right;">
					'.$cert_one.'  '."&nbsp;".'  '.$cert_two.'
					</td>
				</tr>
			</table>
		</td>';
	$html .= '</tr>';
$html .='</table>';
$html .= '<hr/>';
$html .= '<p>';
$html .= apply_filters('the_content', $barristerObj->post_content);
$html .= '</p><br/>';
$html .= $area_of_experience."<br/>".$right_barrister_sidebar;
					

//echo $html;exit;

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser

$dompdf->stream($barristerObj->post_title);
}
?>
<HTML>
	<head>
	<?php wp_head();?>
	</head>
	<BODY onload="javascript:  window.print();">
		<br/><br/>
		<table style="width:100%">
			<tr>
				<td width="5%">
				</td>
				<td colspan="4">
					<img src="<?php echo get_template_directory_uri();?>/img/logo-header.png"/>
				</td>
			</tr>
			<tr>
				<td colspan="5"><br/></td>
			</tr>
			<tr>
				<td width="5%">
				</td>
				<td width="25%">
					<?php 
                    
                    
                    
                    if(get_the_post_thumbnail($barristerId, 'medium')){
                         echo get_the_post_thumbnail($barristerId, 'medium');
                    }else{
                        echo '<img src="' . get_template_directory_uri() . '/img/red pic.png"/>';
                    }
                    ?>
				</td>
				<td width="30%">
					<?php echo '<h1>'. $barristerObj->post_title. '</h1>';?><br/>
					
					<?php 
					
						echo 'Call:'.$call.'<br/>';
						if($silk){
							echo 'Silk:'.$silk.'<br/>';
						}
						if($phone){
							echo 'Phone:'.$phone.'<br/>';
						}
						if($email){
							echo 'Email:'.$email.'<br/>';
						}
						if($honorary_door_tenant == 'Yes'){
							echo 'Honorary Door Tenant<br/>';
						}
					?>
				
				
				</td>
				<td width="25%">
					<?php if($certificate_one){?>
						<img src="<?php echo wp_get_attachment_image_url(  get_field('certificate_one',$barristerId),  'medium' ); ?>" width="100" height="130" />
					<?php }if($certificate_two){?>
						<img src="<?php echo wp_get_attachment_image_url(  get_field('certificate_two',$barristerId),  'medium' ); ?>" width="100" height="130" />
					<?php }?>
				</td>
				<td width="5%">
				</td>
			</tr>
			<tr>
				<td width="5%"></td>
				<td colspan="3"><hr/></td>
				<td width="5%"></td>
			</tr>
			<tr>
				<td width="5%">
				</td>
				<td colspan="3">
					<?php echo apply_filters('the_content', $barristerObj->post_content);?><br/>
					<?php echo $area_of_experience; ?><br>
					<?php echo $right_barrister_sidebar; ?>
					<?php echo $left_barrister_sidebar; ?>
				</td>
				<td width="5%">
				</td>
			</tr>
		</table>
		<br/><br/>
	</BODY>
</HTML>
<?php wp_footer(); ?> 
