<?php 

/**
*
* Displaying Single Barrister page 
* @package bootstrap-basic
* 
*
*/ 
	get_header(); 
	$silk = get_post_meta(get_the_ID(), 'Silk', true);
	wp_enqueue_style('main-style', get_template_directory_uri() . '/css/single_barrister_second.css');


?>

<style>
/*
    .shortlist-opened,
    .shortlist-closed {
        background: #F3F3F3 url(<?php echo get_template_directory_uri();
        ?>/img/shortlist-arrow2.png);
    }
*/
    
    .shortlist-header {
        height: 50px;
        background: rgba(205, 39, 22, 0.6) url(<?php echo get_template_directory_uri();
        ?>/img/shortlist-arch2.png);
    }
    
    #barrister-contact-info {
        background-image: url('<?php echo get_template_directory_uri();?>/img/Group-6.png');
    
    }
    
    .social-network {
        background-image: url('<?php echo get_template_directory_uri();?>/img/oval.png');
        background-repeat: no-repeat;
    }
    
   
 
</style>
<script>
    function resizeSingleBarristerHeader(){
        var windowWidth = window.innerWidth,
            thumbnailHeight = $("#barrister-thumbnail").height();

        if(windowWidth > 992){
            $("#barrister-contact-info").css('height', thumbnailHeight)
        }else{
            $("#barrister-contact-info").css('height', 'auto')
        }
    }
    
    $(document).ready(function(){
        resizeSingleBarristerHeader()
        $(window).resize(resizeSingleBarristerHeader);
        
    });
    
</script>
<div id="barristerHeader">
   <?php if( get_the_post_thumbnail_url()){ ?>
        <div class="col-md-4 nopadd" >
            <div class="relative">
                <img src="<?php echo get_the_post_thumbnail_url();?>" class="img-responsive" id="barrister-thumbnail" alt="<?php the_title(); ?>">
                <div id="mobile-info" class=" col-xs-12 visible-xs">
                    <h1> <?php the_title(); ?> </h1>
                    <strong>Call:</strong>
                    <?php the_field('Call'); ?>
                    <?php if($silk){ ?>&nbsp;&nbsp;&nbsp;
                    <strong>Silk:</strong>
                    <?php echo $silk;?>
                    <?php }?>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="nopadd <?php if( get_the_post_thumbnail_url()){ ?>col-md-8 <?php }else{ ?> col-sm-12 <?php } ?>">
    <div id="barrister-contact-info">
         <div class="dTable">
           <div id="barristerCaption">
            <?php 
               $image_certificate_two = get_field('certificate_two');
                $image_certificate_one = get_field('certificate_one');
                if($image_certificate_one or $image_certificate_two){
                
               ?>
                    <div class="col-sm-4 col-md-12 col-lg-5 hidden-xs hidden-md">
                        <div class="row">
                            <?php 
                           $image_certificate_two = get_field('certificate_two');
                            $image_certificate_one = get_field('certificate_one');
                           if($image_certificate_two){

                           ?>

                            <div class="col-xs-6">
                                <img src="<?php  echo wp_get_attachment_image_url( $image_certificate_two,  'medium' ); ?>" alt="barrister-certificate" class="barrister-certificate pull-right">
                             </div>
                            <?php } ?>
                            <div class="col-xs-6">
                                <?php 
                                if($image_certificate_one){

                                ?>
                                <img src="<?php echo wp_get_attachment_image_url( $image_certificate_one,  'medium' ); ?>" alt="barrister-certificate"  class="barrister-certificate pull-left">
                                <?php } ?>
                            </div>
                        </div>
                    </div>
           <?php } ?>
            
            <div class="<?php if($image_certificate_one or $image_certificate_two){echo 'col-sm-8  col-lg-7 ';}?> col-md-12 hidden-xs">
                <div class="row">
                    <div class="col-sm-6" id="tel-mail-container">
                        <div id="tel-mail">
                            <strong> T: </strong>
                            <a href="tel:<?php echo str_replace(' ', '',  get_field('phone')) ?>">
                                <?php echo get_field('phone')?>
                            </a><br/>
                            <strong>E: </strong>
                            <a href="mailto:<?php echo get_field('email')?>">
                                <?php echo get_field('email')?>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6 socials-container">
                        <a href="mailto:<?php echo get_field('email')?>"> 
                            <span class="social-network">
                                <img src="<?php echo get_template_directory_uri();?>/img/share.png" class="" alt="shareIcon">
                            </span>
                        </a>
                        <a href="/our-people/print-barrister/?id=<?php echo get_the_ID() ?>&pdf=true" target="_blank" title="<?php echo get_field('cv')['title']?>">
                            <span class="social-network">
                                <img src="<?php echo get_template_directory_uri();?>/img/download.png" class="" alt="downloadIcon">
                            </span>
                        </a>
                        <a href="/print-barrister/?id=<?php echo get_the_ID() ?>" target="_blank"> 
                            <span class="social-network">
                                <img src="<?php echo get_template_directory_uri();?>/img/printer.png" class="" alt="printerIcon">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-7 visible-xs">
                <div class="row">
                    <div class="col-sm-6 socials-container">
                        <div class="col-xs-4">
                           <span>
                                <a href="mailto:<?php echo get_field('email')?>"> 
                                    <span class="social-network">
                                        <img src="<?php echo get_template_directory_uri();?>/img/share.png" class="" alt="shareIcon">
                                    </span> 
                                    Share Profile
                                </a>
                            </span>
                        </div>
                       <div class="col-xs-4">
                           <span>
                                <a href="mailto:<?php echo get_field('email')?>"> 
                                    <span class="social-network">
                                        <img src="<?php echo get_template_directory_uri();?>/img/mail_wht.png" class="" alt="shareIcon">
                                    </span>
                                     Email Us
                                </a>
                            </span>
                        </div>
                        
                        <div class="col-xs-4">
                            <span>
                                <a href="/print-barrister/?id=<?php echo get_the_ID() ?>" target="_blank"> 
                                    <span class="social-network">
                                        <img src="<?php echo get_template_directory_uri();?>/img/phone-call-wht.png" class="" alt="printerIcon">
                                    </span>
                                    Call Us
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
          </div><!--#barristerCaption-->
       </div>
     </div> <!--#barrister-contact-info-->
    </div>
    <div class="clearfix"></div>
</div><!--#barristerHeader-->
<div id="baristerTitleContainer" class="hidden-xs">
    <h1 id="baristerName">
        <?php the_title(); ?>
    </h1>
    <div id="captionCallSilk">
        <strong>Call:</strong>
        <?php the_field('Call'); ?>
        <?php if($silk){ ?>&nbsp;&nbsp;&nbsp;
        <strong>Silk:</strong>
        <?php echo $silk;?>
        <?php }?>

    </div>
    <div id="barristerSortlist" class="visible-lg">
        <span class="glyphicon glyphicon-chevron-down" data-toggle="collapse" data-target="#shortlistContainer"></span>
        <div id="shortlist-star" onclick="addToShortlist(<?php the_ID();?>)">Add to Portfolio <div id="shortlist-barrister-num" ></div></div>
    </div><!--End #barristerSortlist-->
    <div id="shortlistContainer" class="visible-lg- collapse">
            <div class="col-sm-12 shortlist-header"> <?php _e('Barrister Shortlist', 'bootstrap-basic'); ?></div>
            <div class="paddingL50">
                <div class="col-sm-12 padding5">
                    <div class="col-sm-4 shortlist-title">Barrister</div>
                    <div class="col-sm-8 shortlist-title text-center">
                        <div class="col-sm-3 padding0">Call</div>
                        <div class="col-sm-3 padding0">CV</div>
                        <div class="col-sm-3 padding0">Email</div>
                        <div class="col-sm-3 padding0">Print</div>
                    </div>
                </div>

                <div class="col-sm-12 padding5">
                    <hr/>
                </div>
                <div id="shortlist"></div>
                <div class="col-sm-12 padding5">
                    <hr/>
                </div>
                <div class="col-sm-12 padding5" id="shortlist-actions">
                    <div class="col-sm-4 padding0">
                        <span id="print-all"><?php _e('Print All', 'bootstrap-basic'); ?></span>
                    </div>
                    <div class="col-sm-4">
                        <span id="email-all"><?php _e('Email All', 'bootstrap-basic'); ?></span>
                    </div>
                    <div class="col-sm-4 text-right padding0">
                        <span onclick="removeFromShortlist()" id="remove-all-barristers">
                            <strong><?php _e('Remove All', 'bootstrap-basic'); ?></strong>
                        </span>
                    </div>
                </div>

                <div class="col-sm-12 padding5">
                    <span id="shortlist-desc"><?php _e('For additional information, please call our clerks on <strong>0207  520 6000</strong>', 'bootstrap-basic'); ?></span>
                </div>
                <div class="col-sm-12 padding10"></div>
            </div>
        </div><!--End #shortlistContainer-->
</div><!--End #baristerTitleContainer-->

<div class="padding20 hidden-xs"></div>
<div class="clearfix"></div>
<div class="col-sm-12" id="BarristerContent">
    <div id="description">
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                the_content();
            }
        }  
    ?>
    </div>

    <div class="clearfix"></div> <br>
    <div class="row">
        <div class="col-xs-12 col-sm-6 BarristerAreaOfExperience">
            <?php the_field('area_of_experience'); ?>
        </div>

        <div class="col-xs-12 col-sm-6 BarristerAreaOfExperience">
            <?php the_field('area_of_experience_right'); ?>
        </div>
    </div>

    <div class="clearfix"></div> <br>
    <div class="row" id="BarristerSidebar">
        <div class="col-xs-12 col-sm-6">
            <?php the_field('right_barrister_sidebar'); ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?php the_field('left_barrister_sidebar'); ?>
        </div>
    </div>

</div>
<script>
    $(document).ready(function() {
        populateShortlist();
    });

    function addToShortlist(baristerId) {

        $.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: "POST",
            cache: false,
            data: 'ID= ' + baristerId + '&action=add_to_shortlist',
            dataType: 'html',
            success: function(html) {
                populateShortlist();
            },
            error: function(html) {}
        });
    }

    function removeFromShortlist(baristerId) {

        $.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: "POST",
            cache: false,
            data: 'ID= ' + baristerId + '&action=remove_from_shortlist',
            dataType: 'html',
            success: function(html) {
                populateShortlist();
            }
        });
    }

    function populateShortlist() {

        $.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: "POST",
            cache: false,
            data: 'action=get_shortlist',
            success: function(barristers) {
                countShortlist();
                var barristersObj = JSON.parse(barristers)

                if (jQuery.isEmptyObject(barristersObj)) {
                    $('#shortlist-actions').slideUp();
                } else {
                    $('#shortlist-actions, #shortlist-barrister-num').slideDown();
                    $('.remove-barrister').css('display', 'none');
                }

                $("#shortlist").empty();
                $.each(barristersObj, function(key, barrister) {
                    $("#shortlist").append('<div class="col-sm-12 padding5">' +
                        '<div class="col-sm-4 shortlist-barrister">' +
                        '<a href="' + barrister.permalink + '" class="shortlist-barrister-link">' +
                        barrister.name +
                        '</a>' +
                        '</div>' +
                        '<div class="col-sm-8 shortlist-barrister text-center">' +
                        '<div class="col-sm-3 padding0">' + barrister.call + '</div>' +
                        '<div class="col-sm-3">' +
                        '<a href="/our-people/print-barrister/?id=' + barrister.barister_id + '&pdf=true" class="shortlist-barrister-link text-center">' +
                        '<i class="fa fa-file-text-o fontS1HalfEM" aria-hidden="true"></i>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                        '<a href="mailto:' + barrister.email + '" class="shortlist-barrister-link text-center">' +
                        '<i class="fa fa-envelope-o fontS1HalfEM" aria-hidden="true"></i>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                        '<a href="/our-people/print-barrister/?id=' + barrister.barister_id + '" class="shortlist-barrister-link text-center" target="_blank">' +
                        '<i class="fa fa-print fontS1HalfEM" aria-hidden="true"></i>' +
                        '</a>' +
                        '</div>' +
                        '</div>' +
                        '<span class="remove-barrister" onclick="removeFromShortlist(' + barrister.barister_id + ')">' +
                        '<img src="<?php echo get_template_directory_uri();?>/img/remove-icon.png" height="20">' +
                        '<span>' +
                        '</div>');
                });

            },
            error: function(response) {

            }
        });
    }

    function countShortlist() {
        $.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: "POST",
            cache: false,
            data: 'action=count_shortlist',
            dataType: 'html',
            success: function(num) {

                if (num > 0) {
                    $('#shortlist-barrister-num').slideDown();
                    $('#shortlist-barrister-num').empty();
                    $('#shortlist-barrister-num').text(num);
                } else {
                    $('#shortlist-barrister-num').slideUp();
                }
            }
        });

    }


    $(document).ready(function() {
        $(".collapseomatic").click(function() {
            $(this).find("span").toggleClass("glyphicon-plus glyphicon-minus")
        });
    });

</script>

<?php get_footer(); ?>
