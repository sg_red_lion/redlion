<?php 
/**
*
* Displaying Barrister page 
* @package bootstrap-basic
* Template Name: Barristers page 
*
*/ 
	//error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	
	get_header(); 

	$honorary['honorary_clause'] = array(
			'key'          => 'honorary_door_tenant',
			'value'        => 'no',
		);
	if(isset($_POST['direct_access'])){	
		$directAccess['direct_access_clause'] = array(
			'key'          => 'direct_access',
			'value'        => 'Yes',
		);
	}
	$barrister_junior_args = array(
		'post_type' => 'barrister',
		'post_status' => 'publish',
		'posts_per_page' => '1000',
		'meta_query'       => array(	
			//comparison between the inner meta fields conditionals
			'relation'    => 'AND',
			//meta field condition one
			'seniority_clause' =>array(
				'key'          => 'seniority',
				'value'        => 'Junior',
			),
			
			 $honorary,
			 $directAccess,
			//meta field condition one
			  'call_clause' => array(
				 'key'          => 'Call',
			 )
			
		),

	);
	$barrister_queens_args = array(
		'post_type' => 'barrister',
		'post_status' => 'publish',
		'posts_per_page' => '1000',
	    'meta_query'       => array(	    
			//comparison between the inner meta fields conditionals
			'relation'    => 'AND',
			//meta field condition one
			'seniority_clause' =>array(
				'key'          => 'seniority',
				'value'        => 'Queens',
			),
			$honorary,
			$directAccess,
			//meta field condition one
			  'call_clause' => array(
				 'key'          => 'Call',
			 ),
             array(
                'relation' => 'OR',
                array(
                    'key'     => 'priority_order',
              
                )
             ),
		),
	); 
	if(!empty($_POST['practice'])){
		$barrister_queens_args['tax_query'][] = array(
										   'taxonomy' => 'practice-area',
											'field' => 'id',
											'terms' => (int)$_POST['practice'],
											'operator' => 'IN'
										);
		$barrister_junior_args['tax_query'][] = array(
										   'taxonomy' => 'practice-area',
											'field' => 'id',
											'terms' => (int)$_POST['practice'],
											'operator' => 'IN'
										);
	}
	
	$searchString = htmlspecialchars($_POST['searchString']);
	
	if($searchString){
		$barrister_queens_args['s'] =  $searchString; 
		$barrister_junior_args['s'] =  $searchString; 			
	}
	
	if(isset($_POST['order'])){
		$barrister_queens_args['orderby'] =  array( 'meta_value_num' => 'ASC',  htmlspecialchars($_POST['order']) => 'ASC' ); 
        $barrister_queens_args['meta_key'] = 'priority_order'; 
		
		$barrister_junior_args['orderby'] = array( 'meta_value_num' => 'ASC',  htmlspecialchars($_POST['order']) => 'ASC' );
        $barrister_junior_args['meta_key'] = 'priority_order'; 
	}else{
        $barrister_queens_args['meta_key'] = 'priority_order'; 
		$barrister_queens_args['orderby'] = 'meta_value_num'; 
		$barrister_queens_args['order'] = 'ASC'; 

		$barrister_junior_args['meta_key'] = 'priority_order'; 
		$barrister_junior_args['orderby'] = 'meta_value_num'; 
		$barrister_junior_args['order'] = 'ASC'; 
	}
	
	$barrister_juniors = new WP_Query( $barrister_junior_args ); 
	$barrister_queens = new WP_Query( $barrister_queens_args ); 
//	 $posts = $barrister_queens->posts;
//	 var_dump($posts);
//   echo $barrister_queens->request;


?>


<div class="barttiersHeader" style="background: #F3F3F3 url(<?php echo get_the_post_thumbnail_url(); ?>); background-size: cover;">	
	<div class="imgCaption">
		<div class="col-xs-6 col-sm-4 padding0">
			<span class="title">
				<?php the_title(); ?>
				<div class="titleLine"></div>
			</span>
		</div>
		<div class="col-xs-6 col-sm-8 padding0 descriptionContainer">
			<span class="description">
				<span class="hidden-xs">
					<?php the_content(); ?>
				</span>	
			</span>	
		</div>
	</div>
</div><!--End .barttiersHeader-->
<div class="clearfix"></div>
	<div class="contariner-fluid content-area" id="main-column">
		<div class="col-md-12 marginT-15">
			<div class="clearfix"></div><br/>
				<div class="col-sm-12 padding0">
					<div class="col-sm-12 seniorityTitle">
						<div class="col-sm-3 padding0 hidden-xs">
							<div class="redLine col-sm-4 padding5"></div>
							<div class="clearfix"></div>
							<?php _e("Queen's Counsel", 'bootstrap-basic'); ?>
						</div>
						<div class="col-sm-9 text-right padding0">
							<form role="search" method="POST" id="barristerFilter" action="<?php echo get_permalink(); ?>">
								<div class="col-sm-2 text-right paddingL0 visible-lg">
									<label for="direct_access" class="btn-label marginT5">
										Direct Access
										<input type="checkbox" name="direct_access" id="direct_access" <?php if(isset($_POST['direct_access']))echo 'checked';?>>
									</label>
								</div>
								<div class="col-xs-6 col-sm-3 col-md-2 text-right paddingL0">
                                    <select name="practice"  onChange="this.form.submit();" class="width100">
                                        <option value="" >Please Select</option>
                                        <?php 
                                            $practiceAreaTerms = get_terms( array(
                                                    'taxonomy' => 'practice-area',
                                                    'hide_empty' => false,
                                                )
                                            );	
                                            foreach ( $practiceAreaTerms as $term ) {
                                                $selected = ($_POST['practice'] == $term->term_id) ? 'selected' : '';
                                                echo '<option value="' . $term->term_id . '" '.$selected.' >'.$term->name .'</option>';
                                            }
                                        ?>	
                                    </select>
								</div>
								
								<div class="col-xs-6 col-sm-2 padding0">
									<div class="form-group">
										<div class="inner-addon left-addon">
											<i class="glyphicon glyphicon-search"></i>
											<input type="text" class="col-xs-12 padding0-xs" id="searchString" placeholder="Search by name, sector, service, location or keyword" value="<?php if(isset($_POST['searchString'])) echo $_POST['searchString']; ?>" name="searchString"> 
										</div>
									</div>
								</div>
								<div class="col-xs-12  col-sm-6 text-center sortBaristerFilter padding0-xs">
                                    <div class="row">
                                    <div class="col-sm-7">
                                        <hr class="visible-xs padding0"/>
                                        <div class="form-group">
                                            <div class="col-xs-2 col-sm-3 col-md-3 padding5 sortByField"><?php _e('Sort By', 'bootstrap-basic'); ?></div>
                                            <select name="order" class="col-xs-7 col-sm-9 col-md-9" onChange="this.form.submit();">
                                                <option value="call_clause" <?php if($_POST['order'] == 'call') echo 'selected'; ?>>
                                                    <?php _e('Seniority', 'bootstrap-basic'); ?>
                                                </option>
                                                <option value="title" <?php if($_POST['order'] == 'title') echo 'selected'; ?>>
                                                    <?php _e('Name', 'bootstrap-basic'); ?>
                                                </option>
                                            </select>
                                            <div class="col-xs-3 col-sm-12 text-right padding0 hidden-lg">
                                                <label for="direct_access" class="btn-label line-height-nrm">
                                                    <input type="checkbox" name="direct_access" id="direct_access" <?php if(isset($_POST['direct_access']))echo 'checked';?>>
                                                    Direct Access

                                                </label>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="clearfix visible-xs"></div>
                                        <div class="col-sm-5 hidden-xs paddingR0">
                                            <div class="padding5 visible-xs"></div>
                                                <input type="submit" name="submit_all" class="btn search-all-members" value="<?php _e('Search', 'bootstrap-basic'); ?>">
                                                <a href="" class="btn search-all-members"><?php _e('Reset', 'bootstrap-basic'); ?></a>
                                            <!--<div class="col-xs-8 col-sm-8 padding0-xs">
                                                 <?php //<input type="submit" name="submit_honorary" class="btn btn-honorary-door-tenants" value="<?php _e('Honorary Door Tenants', 'bootstrap-basic'); ?>">
                                            </div>-->
                                        </div>
                                    </div>
								</div>
								
							</form>
						</div><!--End .col-sm-9 .text-right-->
					</div><!--End .seniorityTitle-->
				</div><!--End .col-sm-12-->

				<div class="col-xs-12 padding0-xs">
					<div class="col-xs-12 seniorityTitle visible-xs">
						<?php _e("Queen's Counsel", 'bootstrap-basic'); ?>
					</div>
					<div class="padding15 col-sm-12"></div>
					<div class="col-sm-12 col-md-2"></div>
					<div class="col-sm-12 col-md-8 padding0-xs">
						<div class="col-sm-6 padding0 paddingR90-sm">
							<div class="col-xs-12 padding0">
								<div class="co-xs-12 titleDevider-xs"></div>
								<div class="nameTitle padding0 col-xs-8">
									<?php _e("Name", 'bootstrap-basic'); ?>
									<div class="clearfix"></div>
									<div class="underlineNameTitle"></div>
									<div class="clearfix"></div>
								</div>
								<div class="silkTitle col-xs-2">
									<?php _e("Silk", 'bootstrap-basic'); ?>
								</div>
								<div class="callTitle col-xs-2">
									<?php _e("Call", 'bootstrap-basic'); ?>
								</div>
							</div>
							<div class="col-xs-12 padding5"></div>
						
						<?php 
							if(($barrister_queens->have_posts())) {
								$queensNum = $barrister_queens->post_count;
								$queensHalf = (int)($queensNum / 2);
								$i = 0;
						
							while ( $barrister_queens->have_posts() ) : $barrister_queens->the_post(); 
 
								if ($i <= $queensHalf) {
									?>								
									<div class="col-xs-8 padding0">
										<a class="barrister-name-link" data-name="<?php the_title();?>"  data-image="<?php echo get_the_post_thumbnail_url();?>" href="<?php echo get_permalink();?>">
										<?php the_title();?></a>
									</div>
									<div class="col-xs-2 silkValue">
										<?php the_field('Silk'); ?>
									</div>
									<div class="col-xs-2 callValue">
										<?php the_field('Call'); ?>
									</div>
							<?php
								}else{ 
									if($i == $queensHalf + 1 ){ ?>
						</div>
						<div class="col-sm-6 padding0 paddingR90-sm">
								<div class="col-sm-12 padding0 hidden-xs"> 
									<div class="co-sm-12"></div>
									<div class="nameTitle padding0 col-sm-8">
										<?php _e("Name", 'bootstrap-basic'); ?>
										<div class="clearfix"></div>
										<div class="underlineNameTitle"></div>
										<div class="clearfix"></div>
									</div>
									<div class="silkTitle col-sm-2">
										<?php _e("Silk", 'bootstrap-basic'); ?>
									</div>
									<div class="callTitle col-sm-2">
										<?php _e("Call", 'bootstrap-basic'); ?>
									</div>
								</div>
								<div class="col-xs-12 padding5 hidden-xs"></div>
								<?php } ?>
									
									<div class="col-xs-8 padding0">
										<a class="barrister-name-link" data-name="<?php the_title();?>"  data-image="<?php echo get_the_post_thumbnail_url();?>" href="<?php echo get_permalink();?>">
										<?php the_title(); ?>
										</a>
									</div>
									<div class="col-xs-2 silkValue">
										<?php the_field('Silk'); ?>
									</div>
									<div class="col-xs-2 callValue">
										<?php the_field('Call'); ?>
									</div>
								<?php	
								}
								$i++;
							
								endwhile; 
								}else{
							?>
						</div>
						
							<?php } ?>
						<div class="clearfix hidden-xs"></div>
						<div class="col-sm-12">
							<?php  if(!$barrister_queens->have_posts()) { _e("No queen's found", 'bootstrap-basic'); } ?>
							<img src="<?php echo get_template_directory_uri();?>/img/red rectangle .png" class="marginR-50 pull-right hidden-sm hidden-xs" alt="red rectangle">
						</div>
					</div>
					<div class="col-sm-12 col-md-2"></div>
				</div><!--End .col-sm-12-->
				
				<div class="clearfix"></div><br/>
				
				

				<?php wp_reset_postdata(); ?>
				<div class="clearfix"></div><br/>
	
				<!--/////////START Juniors/////////////-->
			
					<div class="col-xs-12 seniorityTitle padding0">
						<div class="col-sm-2 padding0">
							<div class="redLine col-sm-6 padding5 hidden-xs"></div>
							<div class="clearfix"></div>
							<?php _e('Junior Counsel', 'bootstrap-basic'); ?>
						</div>
					</div>
					<div class="padding15 col-sm-12"></div>
					<div class="col-sm-12 col-md-2"></div>
					<div class="col-sm-12 col-md-8 padding0-xs">
						<div class="col-sm-6 padding0 paddingR90-sm">
							<div class="col-xs-12 padding0">
								<div class="co-xs-12 titleDevider-xs"></div>
								<div class="nameTitle padding0 col-xs-8">
									<?php _e("Name", 'bootstrap-basic'); ?>
									<div class="clearfix"></div>
									<div class="underlineNameTitle"></div>
									<div class="clearfix"></div>
								</div>
								<div class="silkTitle col-xs-2">
									<?php //_e("Silk", 'bootstrap-basic'); ?>
								</div>
								<div class="callTitle col-xs-2">
									<?php _e("Call", 'bootstrap-basic'); ?>
								</div>
							</div>
							<div class="col-xs-12 padding5"></div>
						
						<?php 
							if(($barrister_juniors->have_posts())) {
								$juniorsNum = $barrister_juniors->post_count;
								$juniorsHalf = (int)($juniorsNum / 2);
								$i = 0;
						
							while ( $barrister_juniors->have_posts() ) : $barrister_juniors->the_post(); 

								if ($i <= $queensHalf) {
									?>								
									<div class="col-xs-8 padding0">
										<a class="barrister-name-link" data-name="<?php the_title();?>"  data-image="<?php echo get_the_post_thumbnail_url();?>" href="<?php echo get_permalink();?>"><?php the_title(); ?></a>
									</div>
									<div class="col-xs-2 silkValue">
										<?php //the_field('Silk'); ?>
									</div>
									<div class="col-xs-2 callValue">
										<?php the_field('Call'); ?>
									</div>
							<?php
								}else{ 
									if($i == $juniorsHalf + 1 ){ ?>
						</div>
						<div class="col-sm-6 padding0 paddingR90-sm">
							<div class="col-sm-12 padding0 hidden-xs"> 
								<div class="co-sm-12"></div>
								<div class="nameTitle padding0 col-sm-8">
									<?php _e("Name", 'bootstrap-basic'); ?>
									<div class="clearfix"></div>
									<div class="underlineNameTitle"></div>
									<div class="clearfix"></div>
								</div>
								<div class="silkTitle col-sm-2">
									<?php //_e("Silk", 'bootstrap-basic'); ?>
								</div>
								<div class="callTitle col-sm-2">
									<?php _e("Call", 'bootstrap-basic'); ?>
								</div>
							</div>
							<div class="col-xs-12 padding5 hidden-xs"></div>
							<?php } ?>
									
								<div class="col-xs-8 padding0">
									<a class="barrister-name-link" data-name="<?php the_title();?>" data-image="<?php echo get_the_post_thumbnail_url();?>" href="<?php echo get_permalink();?>"><?php the_title();?></a>
								</div>
								<div class="col-xs-2 silkValue">
									<?php the_field('Silk'); ?>
								</div>
								<div class="col-xs-2 callValue">
									<?php the_field('Call'); ?>
								</div>
								<?php	
								}
								$i++;
							
								endwhile; 
								}else{
							?>
						</div>
						
						<?php } ?>

						<div class="col-sm-12">
							<?php  if(!$barrister_juniors->have_posts()) { _e("No junior's found", 'bootstrap-basic'); } ?>
							<img src="<?php echo get_template_directory_uri();?>/img/red rectangle .png" class="marginR-50 pull-right hidden-sm hidden-xs" alt="red rectangle">
						</div>
					</div>
					<div class="col-sm-12 col-md-2"></div>
			
				</div><!--End .col-md-12-->
				<div class="clearfix"></div><br/>

				<?php wp_reset_postdata(); ?>
				<div class="clearfix"></div><br/>
				<!--/////////  END Juniors  /////////////-->

	</div><!--End #main-column>-->
</div>
    <script>
        $(document).ready(function(){
            $( ".barrister-name-link" )
              .mouseover(function() {
                    if ($(window).width() > 1200) {
                        var imageSrc = $(this).data("image");
                        var name =  $(this).data("name");
                        if(!imageSrc){
                            imageSrc = '<?php echo get_template_directory_uri();?>/img/red pic.png';
                        }
                        $('#hoveredBaristerImage').attr("src", imageSrc);
                        $('#hoveredBaristerName').text(name);
                        $( "#hoveredBaristerData").css('display','block');
                    }
              })
              .mouseout(function() {
                $( "#hoveredBaristerData").css('display','none');
              });
        });
    </script>
    <div id="hoveredBaristerData">
        <img id="hoveredBaristerImage" src="" alt="barrister name"/>
        <div id="hoveredBaristerName"></div>
    </div>
	
	<?php get_footer(); ?> 