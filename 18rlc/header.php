<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
   
	<?php wp_head(); ?> 
   
	<script type="text/javascript">
		jQuery(document).ready(function($){
		
			$('#backToTop').on('click', function(){
				$('html, body').animate({scrollTop:0}, 1500);
				return false;
			});
			$('.header-search-icon, .header-phone-icon, #mega-menu-wrap-primary').click(function () {
				$( '#mega-menu-primary' ).css( 'display','none' );
			    $( '#header-contact' ).removeClass( 'in' );
			    $( '#searchContainer' ).addClass( 'hidden-xs' );
			});
			
		    $('.header-search-icon').click(function () {
			   $( '#searchContainer' ).removeClass( 'hidden-xs' );
		    });
			
			$('.header-phone-icon').click(function () {
			   $( '#header-contact' ).removeClass( 'in' );
			});
			
			$('#mega-menu-wrap-primary').click(function () {
			   $( '#mega-menu-primary' ).css( 'display','block' );
			});
		});
  </script>

    <?php 
    if (0) {
    $FontDefinition = '';
    $FontURL = '';

    if(isset($_GET['font-definition'])){ $FontDefinition = $_GET['font-definition'];}
    if(isset($_GET['font-url'])){ $FontURL = $_GET['font-url'];}
    if($FontURL and $FontDefinition){
        ?>
        <style>
             @import url('<?php echo $FontURL;?>');
            body,header{
               <?php echo stripslashes($FontDefinition);?>
            }
        </style>
        <?php

    }
    }
    ?>
    <style>
       /*  @import url('https://fonts.googleapis.com/css?family=Halant&font-definition=font-family:%27Halant%27,serif;');
        body{
           font-family:'Halant',serif !important;
        }*/
    </style>
</head>
<body <?php body_class();?> >
		<!--[if lt IE 8]>
			<p class="ancient-browser-alert">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a>.</p>
		<![endif]-->

  	<?php do_action('before'); ?> 
	<header>
		<div class="container-fluid logoContainer">
			<div class="col-xs-7 col-sm-6 col-md-7">
				<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
					<img src="<?php echo get_template_directory_uri();?>/img/logo-header.png" class="img-responsive logo-header" alt="logo - red lion chambers">
				</a>
			</div>
			<div class="visible-xs col-xs-5 text-right padding0">
				
			</div>
			<div class="col-sm-6 col-md-5 hidden-xs">
				<div class="col-sm-12 marginT10">
					
					<div class="col-md-4 col-lg-2 padding0 pull-right">
						<a href="<?php echo get_option('general_setting_linkedin'); ?>" target="_blank">
							<img src="<?php echo get_template_directory_uri();?>/img/linked.png" class="pull-right socialIcons" alt="linkedIn">
						</a>
					
						<a href="<?php echo get_option('general_setting_twitter'); ?>" target="_blank">
							<img src="<?php echo get_template_directory_uri();?>/img/twitter.png" class="pull-right socialIcons" alt="twitter">
						</a>
					</div>
					<div class="col-md-4 col-lg-3 pull-right padding0">
						<?php echo get_option('setting_header_contact_two'); ?>
					</div>
					<div class="col-md-4 col-lg-3 pull-right padding0">
						<?php echo get_option('setting_header_contact_one'); ?>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-default">
		  <div class="container-fluid" id="menu-container">
			<img class="header-phone-icon visible-xs" data-toggle="collapse" data-target="#header-contact" src="<?php echo get_template_directory_uri();?>/img/header-phone-icon.png" alt="phone"/>
			<img class="header-search-icon visible-xs" src="<?php echo get_template_directory_uri();?>/img/header-search-icon.png" alt="search"/>
			
			 
				<?php 
				//include 'wp_bootstrap_navwalker.php';
				$args = array(
							'theme_location' => 'primary', 
							'container' => false, 
							'menu_class' => 
							'nav navbar-nav', 
						//	'walker' => new wp_bootstrap_navwalker()
						);
				wp_nav_menu($args); 
				?> 
			  
			
		  </div>
		</nav>
		<div class="collapse text-center" id="header-contact">
			<div class="col-xs-6">
				<?php echo get_option('setting_header_contact_two'); ?>
			</div>
			<div class="col-xs-6">
				<?php echo get_option('setting_header_contact_one'); ?>
			</div>
		</div>
		<div id="searchContainer2">
			<form role="search" method="get" class="search-form form" action="<?php echo esc_url(home_url('/')); ?>">
                <input type="hidden" name="meta_key" value="barrister">
                <input type="hidden" name="order" value="ASC">
				<div class="form-group">
					<div class="inner-addon left-addon">
					  <i class="glyphicon glyphicon-search"></i>
					  <input type="text" class="form-control input-sm menuSearch" placeholder="<?php echo esc_attr_x('Search &hellip;', 'placeholder', 'bootstrap-basic'); ?>" value="<?php echo esc_attr(get_search_query()); ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label', 'bootstrap-basic'); ?>">
					</div>
				</div>
			</form>
		</div>
		<div class="hidden-xs container-fluid" id="searchContainer">
			<div class="col-sm-8"></div>
			<div class="col-sm-3 padding0-xs">
				<?php include "searchform.php";?>
			</div>	
			<div class="col-sm-1"></div>
		</div>
	</header>
	<div class="clearfix"></div>
	<div id="content-wrapper">