<?php 

/**
*
* Displaying Single Barrister page 
* @package bootstrap-basic
* 
*
*/ 
	get_header(); 
	$silk = get_post_meta(get_the_ID(), 'Silk', true);
	wp_enqueue_style('main-style', get_template_directory_uri() . '/css/single_barrister.css');


?>

<style>
    .shortlist-opened,
    .shortlist-closed {
        background: #F3F3F3 url(<?php echo get_template_directory_uri();
        ?>/img/shortlist-arrow2.png);
    }
    
    #shortlist-barrister-num {
        background: #F3F3F3 url(<?php echo get_template_directory_uri();
        ?>/img/shortlist-num-background.png);
    }
    
    .shortlist-header {
        height: 50px;
        background: rgba(205, 39, 22, 0.6) url(<?php echo get_template_directory_uri();
        ?>/img/shortlist-arch2.png);
    }

</style>
<script>
    $(document).ready(function() {
        populateShortlist();

    });

    function addToShortlist(baristerId) {

        $.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',    
            type: "POST",
            cache: false,
            data: 'ID= ' + baristerId + '&action=add_to_shortlist',
            dataType: 'html',
            success: function(html) {
                populateShortlist();
            },
            error: function(html) {}
        });
    }

    function toggleShortlist() {
        if ((-1) * parseInt($("#shortlistContainer").css("left").replace("px", "")) == 0) {
            $("#shortlistContainer").animate({
                left: "-500px"
            }).height('315px');
//            $('#shortlist-vertical-title').css('display', 'block');
            countShortlist();
            $('.remove-barrister').css('display', 'none');
            $('#shortlist-container-arrow').removeClass('shortlist-opened');
            $('#shortlist-container-arrow').addClass('shortlist-closed');
        } else {
            $("#shortlistContainer").animate({
                left: "0px"
            }).height('auto');
//            $('#shortlist-barrister-num, #shortlist-vertical-title').css('display', 'none');
            $('.remove-barrister').css('display', 'block');
            $('#shortlist-container-arrow').addClass('shortlist-opened');
            $('#shortlist-container-arrow').removeClass('shortlist-closed');
        }
    }

    function removeFromShortlist(baristerId) {

        $.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',   
            type: "POST",
            cache: false,
            data: 'ID= ' + baristerId + '&action=remove_from_shortlist',
            dataType: 'html',
            success: function(html) {
                populateShortlist();
            }
        });
    }

    function populateShortlist() {

        $.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',   
            type: "POST",
            cache: false,
            data: 'action=get_shortlist',
            success: function(barristers) {
                countShortlist();
                var barristersObj = JSON.parse(barristers)

                if (jQuery.isEmptyObject(barristersObj)) {
                    $('#shortlist-actions').slideUp();
                } else {
                    $('#shortlist-actions, #shortlist-barrister-num').slideDown();
                    $('.remove-barrister').css('display', 'none');
                }

                $("#shortlist").empty();
                $.each(barristersObj, function(key, barrister) {
                    $("#shortlist").append('<div class="col-sm-12 padding5">' +
                        '<div class="col-sm-4 shortlist-barrister">' +
                        '<a href="' + barrister.permalink + '" class="shortlist-barrister-link">' +
                        barrister.name +
                        '</a>' +
                        '</div>' +
                        '<div class="col-sm-8 shortlist-barrister text-center">' +
                        '<div class="col-sm-3 padding0">' + barrister.call + '</div>' +
                        '<div class="col-sm-3">' +
                        '<a href="/our-people/print-barrister/?id=' + barrister.barister_id + '&pdf=true" class="shortlist-barrister-link text-center">' +
                        '<i class="fa fa-file-text-o fontS1HalfEM" aria-hidden="true"></i>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                        '<a href="mailto:' + barrister.email + '" class="shortlist-barrister-link text-center">' +
                        '<i class="fa fa-envelope-o fontS1HalfEM" aria-hidden="true"></i>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                        '<a href="/our-people/print-barrister/?id=' + barrister.barister_id + '" class="shortlist-barrister-link text-center" target="_blank">' +
                        '<i class="fa fa-print fontS1HalfEM" aria-hidden="true"></i>' +
                        '</a>' +
                        '</div>' +
                        '</div>' +
                        '<span class="remove-barrister" onclick="removeFromShortlist(' + barrister.barister_id + ')">' +
                        '<img src="<?php echo get_template_directory_uri();?>/img/remove-icon.png" height="20">' +
                        '<span>' +
                        '</div>');
                });

            },
            error: function(response) {

            }
        });
    }

    function countShortlist() {
        $.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',   
            type: "POST",
            cache: false,
            data: 'action=count_shortlist',
            dataType: 'html',
            success: function(num) {

                if (num > 0) {
                    $('#shortlist-barrister-num').slideDown();
                    $('#shortlist-barrister-num').empty();
                    $('#shortlist-barrister-num').text(num);
                } else {
                    $('#shortlist-barrister-num').slideUp();
                }
            }
        });

    }

</script>
<!--

<div id="shortlistContainer" class="hidden-xs">
    <div id="buttonSlider">
        <div id="shortlist-container-arrow" onclick="toggleShortlist()" class="shortlist-closed"></div>
        <div id="shortlist-vertical-title" onclick="toggleShortlist()">
            <?php _e('Barrister Shortlist', 'bootstrap-basic'); ?>
        </div>
        <div id="shortlist-barrister-num" onclick="toggleShortlist()"></div>
    </div>
    <aside>
        <div class="col-sm-12 shortlist-header">Barrister shortlist</div>
        <div class="paddingL50">
            <div class="col-sm-12 padding5">
                <div class="col-sm-4 shortlist-title">Batister</div>
                <div class="col-sm-8 shortlist-title text-center">
                    <div class="col-sm-3 padding0">Call</div>
                    <div class="col-sm-3 padding0">CV</div>
                    <div class="col-sm-3 padding0">Email</div>
                    <div class="col-sm-3 padding0">Print</div>
                </div>
            </div>

            <div class="col-sm-12 padding5">
                <hr/>
            </div>
            <div id="shortlist"></div>
            <div class="col-sm-12 padding5">
                <hr/>
            </div>
            <div class="col-sm-12 padding5" id="shortlist-actions">
                <div class="col-sm-4 padding0">
                    <span id="print-all"><?php _e('Print All', 'bootstrap-basic'); ?></span>
                </div>
                <div class="col-sm-4">
                    <span id="email-all"><?php _e('Email All', 'bootstrap-basic'); ?></span>
                </div>
                <div class="col-sm-4 text-right padding0">
                    <span onclick="removeFromShortlist()" id="remove-all-barristers">
						<strong><?php _e('Remove All', 'bootstrap-basic'); ?></strong>
					</span>
                </div>
            </div>

            <div class="col-sm-12 padding5">
                <span id="shortlist-desc"><?php _e('For additional information, please call our clearks on <strong>0207  520 6000</strong>', 'bootstrap-basic'); ?></span>
            </div>
            <div class="col-sm-12 padding10"></div>
        </div>
    </aside>
</div> 
-->
<style>

    #barrister-contact-info {
        background-image: url('<?php echo get_template_directory_uri();?>/img/Group-6.png'); 
        background-size: cover;
        background-position: center;
        display: flex;
        flex:1;
        padding: 0;
        margin: 0;
        align-items: center;
        justify-content: center;
    }
    #barristerHeader{
        display: -ms-flex;
        display: -webkit-flex; 
        display: flex;
        padding: 0;
        margin: 0;
    }
    #baristerTitleContainer{
        display: flex;
        padding: 0;
        margin: 0;
        min-height: 85px;
        background-color: #292929;
        color: white;
    }
    
    #baristerName{
        padding: 0 0 0 50px;
    }
    #captionCallSilk{
        font-size: 24px;
        border-left: 1px solid white;
        padding: 0 30px;
        margin: 25px 0px 25px 30px;
    }  
    #barristerSortlist{
        border: 1px solid white;
        color: white;
        background-color: transparent;
        height: 40px;
        margin-top: 20px;
        margin-right: 30px;
        width: 190px;
        text-align: right;
        padding-right: 15px;
        font-size: 18px;
        line-height: 35px;
        right: 0;
        position: absolute;
        cursor: pointer;
    }
    
    #barristerSortlist .glyphicon-chevron-down {
            top: 4px;
            
    }
    .barrister-certificate{
        height: 150px;
        border: 3px solid white;
        margin-right: 15px;
        display: inline;
        float: left;
    }
    #barristerCaption{
        width: 100%;
        padding: 0 30px;
        color: white;
        width: 100%;
        display: flex;
        align-items: center;
        font-size: 17px;
    }
    #barristerCaption a{
        color: white;
    }
    .social-network{
        background-image: url('<?php echo get_template_directory_uri();?>/img/oval.png'); 
        height: 60px;
        width: 60px;
        float: left;
        margin-right: 10px;
        display: inline;
        background-size: cover;
        text-align: center;
        line-height: 55px;
    }
    #BarristerContent{
        background: white;
        margin-bottom: 20px;
        padding: 45px 50px;
        font-size: 16px;
        color: #6A6A6A;
        margin-left: 0;
    }

    
    #BarristerContent #description {
        -moz-column-count: 2;
        -moz-column-gap: 40px;
        -webkit-column-count: 2;
        -webkit-column-gap: 40px;
        column-count: 2;
        column-gap: 40px;
        width: 100%
        width: 100%;
    }
    #socials-container{
        
    }
    #tel-mail-container{
        display: table;
        height: 60px;
    }
    #tel-mail{
         display: table-cell;
        vertical-align: middle;
    }
    .nopadd{
    	padding-left: 0px;
    	padding-right: 0px;
    }
    .w100pr{
    	width: 100%;
    }

</style>


<div class="row" id="barristerHeader">
    <div class="pull-left col-md-4 nopadd">
        <img src="<?php echo get_the_post_thumbnail_url();?>" class="img-responsive w100pr" alt="<?php the_title(); ?>">
    </div>
    <div id="barrister-contact-info">
        <div id="barristerCaption">
           <div class="col-xs-6">
            <?php if(get_field('certificate_two')){?>
                    <img src="<?php echo get_field('certificate_two')['url']?>" class="img-responsive barrister-certificate">
            <?php } if(get_field('certificate_one')){?>
                    <img src="<?php echo get_field('certificate_one')['url']?>" class="img-responsive barrister-certificate">
            <?php } ?>
           </div>
           <div class="col-xs-6">
               <div class="row">
                   <div class="col-sm-6" id="tel-mail-container">
                       <div  id="tel-mail">
                           <strong> T: </strong> <a href="tel:<?php echo get_field('phone')?>"><?php echo get_field('phone')?></a><br/>
                           <strong>E: </strong> <a href="mailto:<?php echo get_field('email')?>"><?php echo get_field('email')?></a>
                       </div>  
                   </div>  
                   <div class="col-sm-6" id="socials-container">
                      <div class="social-network">
                           <img src="<?php echo get_template_directory_uri();?>/img/share.png" class="" alt="shareIcon">
                       </div>
                       <a href="<?php echo get_field('cv')['url']?>" target="_blanck" title="<?php echo get_field('cv')['title']?>">
                           <div class="social-network">
                               <img src="<?php echo get_template_directory_uri();?>/img/download.png" class="" alt="downloadIcon">
                           </div>
                       </a>
                       <div class="social-network">
                          <a href="/print-barrister/?id=<?php echo get_the_ID() ?>"><img src="<?php echo get_template_directory_uri();?>/img/printer.png" class="" alt="printerIcon"></a>
                       </div>

                   </div>
               </div>  
           </div><!--.col-xs-6-->
        </div><!--#barristerCaption-->
    </div><!--#barrister-contact-info-->
    <div class="clearfix"></div>
</div> <!--#barristerHeader-->
<div id="baristerTitleContainer">
    <h1 id="baristerName"><?php the_title(); ?></h1>
    <div id="captionCallSilk">
        <strong>Call:</strong> <?php the_field('Call'); ?>
        <?php if($silk){ ?>&nbsp;&nbsp;&nbsp;
          <strong>Silk:</strong> <?php echo $silk;?>
        <?php }?>

    </div>
    <div id="barristerSortlist">
       <span class="glyphicon glyphicon-chevron-down"></span>
       <span id="shortlist-star">Add to Portfolio</span>  <div id="shortlist-barrister-num" onclick="toggleShortlist()"></div>
    </div>
</div>

<div class="padding20 hidden-xs"></div>
<div class="clearfix"></div>
<div class="col-sm-12" id="BarristerContent">
    <div id="description">
    <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                the_content();
            }
        }  
    ?>
    </div>
    
     <div class="clearfix"></div> <br>
    <div class="row">
        <div class="col-xs-12 col-sm-6 BarristerAreaOfExperience">
            <?php the_field('area_of_experience'); ?>
        </div>

        <div class="col-xs-12 col-sm-6 BarristerAreaOfExperience">
            <?php the_field('area_of_experience_right'); ?>
        </div>
	</div>
	
	<div class="clearfix"></div> <br>
	<div class="row" id="BarristerSidebar">
        <div class="col-xs-12 col-sm-6">
            <?php the_field('right_barrister_sidebar'); ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?php the_field('left_barrister_sidebar'); ?>
        </div>
    </div>

</div>
	
<script>
	$(document).ready(function(){
		$(".collapseomatic").click(function(){
		  $(this).find("span").toggleClass("glyphicon-plus glyphicon-minus")
		});
	});
</script>
<?php get_footer(); ?>
