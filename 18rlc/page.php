<?php
/**
 * Template for displaying pages
 * 
 * @package bootstrap-basic
 */
get_header();

$parentId = wp_get_post_parent_id(get_the_ID()); 

$header_img = get_template_directory_uri().'/img/Group.png';
if(get_the_post_thumbnail_url()){
	$header_img =	get_the_post_thumbnail_url();
}

$subHeaderFont = '';
if(isset($_GET['subHeaderFont'])){ $subHeaderFont = "style=\"font-family:'".$_GET['subHeaderFont']."',serif\"";}
?>

<div class="barttiersHeader" style="background: #F3F3F3 url(<?php echo $header_img; ?>); background-size: cover;">
	<div class="imgCaption">
		<div class="col-xs-12 col-sm-4 padding0">
			<span class="title" <?php echo $subHeaderFont; ?>>
				<?php echo get_the_title()?>			
				<div class="titleLine"></div>
			</span>
		</div>
		<div class="col-xs-6 col-sm-8 padding0  descriptionContainer">
			<span class="description">
			
			</span>	
		</div>
	</div>
</div>


<div class="singleBarristerHeader contentFontProperties" id="main-column">
	<main id="main" class="site-main" role="main">
 		<div class="col-sm-8 col-md-9 padding0-xs" id="page-content"> 
<!--		<div class="col-sm-8 col-md-9 col-lg-7">-->
			<div class="col-sm-12 backWhite padding0-xs">
			
				<?php 
				while (have_posts()) {
					the_post();

					get_template_part('content', 'page');

					echo "\n\n";
					
					// If comments are open or we have at least one comment, load up the comment template
					if (comments_open() || '0' != get_comments_number()) {
						comments_template();
					}

					echo "\n\n";

				} //endwhile;
				?> 
				<br/>
			</div>
		</div>
		

		<?php if(0){?>
			<ul>
				<?php echo $parentId; wp_list_pages("title_li=&child_of=$parentId" ); ?>
			</ul>
		<?php }?>

		<div class="hidden-xs col-sm-4 col-md-3" id="page-sidebar">
			<?php 
			 $args = array(
				'post_type'      => 'page',
				'post__not_in' => array(get_the_ID()),
				'posts_per_page' => -1,
				'post_parent'    => $parentId,
				'order'          => 'ASC',
				'orderby'        => 'menu_order',
				
			 );
			 
			 $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 'About Us', );

		
			 
			if($parentId){
				$parent = new WP_Query( $args );
		
				
				if ( $parent->have_posts() ) : ?>
					<div class="col-sm-12 backWhite"> 
						
							<h3 class="fontS20"><a href="<?php the_permalink($parentId); ?>" title="<?php the_title($parentId); ?>">
								<?php echo get_the_title($parentId).'<br/>';?></a>
							</h3>
						
						<ul id="parentPagesList">
							<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php the_title(); ?>
									</a>
								</li>
							
							<?php endwhile; ?>
						</ul>
					</div>
					<div class="clearfix"></div><br/>
				<?php endif; wp_reset_query(); 	
			
			} ?>
			
			
			
			<?php get_template_part('content', 'rlc-sidebar'); ?>
		</div><div class="clearfix"></div><br/>
	</main>
</div>

<?php get_footer(); ?> 