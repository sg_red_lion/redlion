<?php 

	class RedLionSettingsPage
	{
		/**
		 * Holds the values to be used in the fields callbacks
		 */
		private $options;

		/**
		 * Start up
		 */
		public function __construct()
		{
			add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
			add_action( 'admin_init', array( $this, 'page_init' ) );
		}

		/**
		 * Add options page
		 */
		public function add_plugin_page()
		{
			// This page will be under "Settings"
			add_options_page(
				'Settings Admin', 
				'Red Lion Settings', 
				'manage_options', 
				'my-setting-admin', 
				array( $this, 'create_admin_page' )
			);
		}

		/**
		 * Options page callback
		 */
		public function create_admin_page()
		{
			// Set class property
			//$this->options = get_option( 'my_option_name' );
			?>
			<div class="wrap">
				<h1>Red Lion Settings</h1>
				<form method="post" action="options.php">
				<?php
					// This prints out all hidden setting fields
					settings_fields( 'my_option_group' );
					do_settings_sections( 'my-setting-admin' );
					submit_button();
				?>
				</form>
			</div>
			<?php
		}

		/**
		 * Register and add settings
		 */
		public function page_init()
		{  
		
				
			register_setting('my_option_group','setting_header_contact_one');	
			register_setting('my_option_group','setting_header_contact_two');	
			register_setting('my_option_group','setting_phone');	
			register_setting('my_option_group','setting_email');	
			register_setting('my_option_group','how_are_we_title_setting');	
			register_setting('my_option_group','how_are_we_setting');	
			register_setting('my_option_group','general_setting_twitter');
			register_setting('my_option_group','general_setting_linkedin');
			register_setting('my_option_group','setting_features_one');
			register_setting('my_option_group','setting_features_two');
			register_setting('my_option_group','setting_features_three');
			register_setting('my_option_group','setting_get_in_touch_one');
			register_setting('my_option_group','setting_get_in_touch_two');
			register_setting('my_option_group','setting_get_in_touch_three');

			add_settings_section(
				'setting_section_id', // ID
				'Enter your settings below', // Title
				array( $this, 'print_section_info' ), // Callback
				'my-setting-admin' // Page
			);  
			add_settings_field(
				'setting_header_contact_one', 
				'Header contact first', 
				array( $this, 'setting_header_contact_one_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
			add_settings_field(
				'setting_header_contact_two', 
				'Header contact second', 
				array( $this, 'setting_header_contact_two_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
			add_settings_field(
				'setting_phone', 
				'Contact phone', 
				array( $this, 'setting_phone_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
			
			add_settings_field(
				'setting_email', 
				'Contact email', 
				array( $this, 'setting_email_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
			
			add_settings_field(
				'how_are_we_setting', 
				'How Are We title', 
				array( $this, 'how_are_we_title_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
			
			add_settings_field(
				'how_are_we_title_setting', 
				'How Are We description', 
				array( $this, 'how_are_we_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
			
			add_settings_field(
				'general_setting_linkedin',
				'LinkedIn Page',
				array( $this, 'linkedin_link_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			); 

			add_settings_field(
				'general_setting_twitter',
				'Twitter Account',
				array( $this, 'twitter_link_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);	
			
			add_settings_field(
				'setting_features_one',
				'Feature one',
				array( $this, 'setting_features_one_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
			
			add_settings_field(
				'setting_features_two',
				'Feature two',
				array( $this, 'setting_features_two_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
			
			add_settings_field(
				'setting_features_three',
				'Feature three',
				array( $this, 'setting_features_three_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);			
			add_settings_field(
				'setting_get_in_touch_one',
				'GetInTouch column three',
				array( $this, 'get_in_touch_one_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);	
			add_settings_field(
				'setting_get_in_touch_two',
				'GetInTouch column three',
				array( $this, 'get_in_touch_two_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
			add_settings_field(
				'setting_get_in_touch_three',
				'GetInTouch column three',
				array( $this, 'get_in_touch_three_callback'), 
				'my-setting-admin', 
				'setting_section_id'
			);
		}

		/**
		 * Sanitize each setting field as needed
		 *
		 * @param array $input Contains all settings fields as array keys
		 */
		public function sanitize($input)
		{			
			/*
			$new_input = array();
			if(isset($input['setting_get_in_touch_three']))
							$new_input['setting_get_in_touch_three'] = sanitize_text_field( $input['setting_get_in_touch_three']);

			return $new_input;
			*/
			return sanitize_text_field($input);
		}

		/** 
		 * Print the Section text
		 */
		public function print_section_info()
		{
			print '';
		}

		/** 
		 * Get the settings option array and print one of its values
		 */
		public function  setting_phone_callback()
		{
			echo '<input class="regular-text" name="setting_phone" id="setting_phone" type="text" value="'. get_option('setting_phone') .'" />';
		}
		
		public function  setting_email_callback()
		{
			echo '<input class="regular-text" name="setting_email" id="setting_email" type="text" value="'. get_option('setting_email') .'" />';
		}
		
		public function  how_are_we_title_callback()
		{
			echo '<input class="regular-text" name="how_are_we_title_setting" id="how_are_we_title_setting" type="text" value="'. get_option('how_are_we_title_setting') .'" />';
		}
		public function  how_are_we_callback()
		{
			echo '<textarea class="regular-text" rows="6" name="how_are_we_setting" id="how_are_we_setting" >'. get_option('how_are_we_setting') .'</textarea>';
		}
		public function  setting_header_contact_one_callback()
		{
			echo '<textarea class="regular-text" rows="6" name="setting_header_contact_one" id="setting_header_contact_one" >'. get_option('setting_header_contact_one') .'</textarea>';
		}
		
		public function  setting_header_contact_two_callback()
		{
			echo '<textarea class="regular-text" rows="6" name="setting_header_contact_two" id="setting_header_contact_two" >'. get_option('setting_header_contact_two') .'</textarea>';
		}
		
		public function twitter_link_callback()
		{
			echo '<input class="regular-text" name="general_setting_twitter" id="general_setting_twitter" type="text" value="'. get_option('general_setting_twitter') .'" />';
		}
		
		public function linkedin_link_callback()
		{
			echo '<input class="regular-text" name="general_setting_linkedin" id="general_setting_linkedin" type="text" value="'. get_option('general_setting_linkedin') .'" />';
		}
		public function  setting_features_one_callback()
		{
			echo '<textarea class="regular-text" rows="6" name="setting_features_one" id="setting_features_one" >'. get_option('setting_features_one') .'</textarea>';
		}
		public function  setting_features_two_callback()
		{
			echo '<textarea class="regular-text" rows="6" name="setting_features_two" id="setting_features_two" >'. get_option('setting_features_two') .'</textarea>';
		}
		
		public function  setting_features_three_callback()
		{
			echo '<textarea class="regular-text" rows="6" name="setting_features_three" id="setting_features_three" >'. get_option('setting_features_three') .'</textarea>';
		}
		  
		public function  get_in_touch_one_callback()
		{
			echo '<textarea class="regular-text" rows="6" name="setting_get_in_touch_one" id="setting_get_in_touch_one" >'. get_option('setting_get_in_touch_one') .'</textarea>';
		}
		
		public function  get_in_touch_two_callback()
		{
			echo '<textarea class="regular-text" rows="6" name="setting_get_in_touch_two" id="setting_get_in_touch_two" >'. get_option('setting_get_in_touch_two') .'</textarea>';
		}
		
		public function  get_in_touch_three_callback()
		{
			echo '<textarea class="regular-text" rows="6" name="setting_get_in_touch_three" id="setting_get_in_touch_three" >'. get_option('setting_get_in_touch_three') .'</textarea>';
		}
	}