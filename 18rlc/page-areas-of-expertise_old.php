<?php
/**
 * Template for displaying pages
 * 
 * @package bootstrap-basic
 */
get_header();

$header_img = get_template_directory_uri().'/img/Group.png';
if(get_the_post_thumbnail_url()){
	$header_img =	get_the_post_thumbnail_url();
}
 ?>
    <style>
        #areas-of-expertise-list a {
            color: black;
            text-decoration: none;
        }
        
        #areas-of-expertise-list {
            padding-top: 30px;
        }
        
        #areas-of-expertise-list>li {
            list-style-type: none;
        }
        
        #areas-of-expertise-list>li>a {
            border-width: 0px 0px 7px 0px;
            border-color: #f3f3f3;
            border-style: solid;
            margin: 20px 0;
            font-size: 28px;
            padding: 0;
            display: block;
            padding-bottom: 12px;
        }
        
        ul.children {
            margin-left: -40px;
            -webkit-column-count: 3;
            -moz-column-count: 3;
            column-count: 3;
        }
        
        ul.children>li {
            list-style-type: none;
            font-size: 14px;
            line-height: 30px;
        }
        
        ul.children>li:before {
            display: inline-block;
            content: '\2022';
            padding-right: 15px;
        }
        
        ul.children {
            padding-bottom: 50px;
        }

    </style>
    <div class="barttiersHeader" style="background: #F3F3F3 url(<?php echo $header_img; ?>);">
        <div class="imgCaption">
            <div class="col-xs-6 col-sm-4 padding0">
                <span class="title">
				<?php echo get_the_title()?>				
				<div class="titleLine"></div>
			</span>
            </div>
            <div class="col-xs-6  col-sm-8 padding0  descriptionContainer">
                <span class="description">
			
			</span>
            </div>
        </div>
    </div>

    <div class="container-fluid padding0 singleBarristerHeader contentFontProperties" id="main-column">
        <main id="main" class="site-main" role="main">
            <div class="col-sm-8 col-md-9">
                <div class="col-sm-12 backWhite">
                    <br/>
                    <?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					}
					the_content();
				?>
                        <script>
                            $(document).ready(function() {
                                $("#areas-of-expertise-list > li > a").before('<div class="redLine col-sm-1 padding0"></div><div class="clearfix"></div>');
                            });

                        </script>

                        <div id="areas-of-expertise-list">
                            <?php 
						wp_list_pages( array(
						'child_of' => get_the_ID(),
						'title_li' => '',
						))
					?>
                        </div>
                        <br/>
                </div>
            </div>

            <div class="hidden-xs col-sm-4 col-md-3">
                <div class="col-sm-12 backWhite">
                    <h3 class="view-our-barristers-sidebar">
                        <a href="/barristers">
                            <?php _e('View our Barristers', 'bootstrap-basic'); ?>
                        </a>
                    </h3>
                </div>
                <div class="clearfix"></div><br/>

                <div class="col-sm-12 backWhite">
                    <h3 class="">
                        <?php _e('Contact us', 'bootstrap-basic'); ?>
                    </h3>
                    <br/>
                    <?php 
					if(get_option('setting_email')){
						echo '<div class="col-sm-12 padding0"><img src="' . get_template_directory_uri() . '/img/email icon@2x.png" class="contactSidebarIcons"><a class="black-text" href="mailto:'.get_option('setting_email').'">' . get_option('setting_email').'</a></div>';
					}
					
					if(get_option('setting_phone')){
						echo '<div class="col-sm-12 padding0"><img src="' . get_template_directory_uri() . '/img/call icon@2x.png" class="contactSidebarIcons"><a class="black-text" href="tel:'.get_option('setting_phone').'">' . get_option('setting_phone').'</a></div>';
					} 				?>
                    <div class="clearfix"></div><br/>
                    <a href="/case-enquiry-questionnaires/" class="btn btn-enquire">Enquire</a>
                    <div class="clearfix"></div><br/>
                </div>

            </div>
        </main>
    </div>
    <?php get_footer(); ?>
