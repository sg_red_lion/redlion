<?php 
/**
 * Displaying archive page (category, tag, archives post, author's post)
 * 
 * @package bootstrap-basic
 */

get_header(); 

$searchString = (isset($_POST['searchString'])) ? $_POST['searchString']  : '';

if($_POST){
	
//	  wp_reset_postdata();
    wp_reset_query();
//    rewind_posts();
	$category_args = array(
						//'post_type' => 'barrister',
						'post_status' => 'publish',
						's' => $searchString,
						'posts_per_page' => '30',
						'cat' => get_the_category()[0]->term_id,

					);
	if(!empty($_POST['practice'])){

		$category_args['tax_query'][] = array(
									    'taxonomy' => 'practice-area',
										'field' => 'id',
										'terms' => (int)$_POST['practice'],
										'operator' => 'IN'
									);
	}
//echo '<pre>';var_dump($category_args);echo '</pre>';
	$categoryObj = new WP_Query( $category_args );
//	echo $categoryObj->request;
	//echo $categoryObj->found_posts;
}


	$terms = get_the_terms( get_the_ID(), 'category');
	$header_image = '';
	
	if(!empty($terms)){
		$term = array_pop($terms);
		$header_image = get_field('header_image', $term );
	} 
	
	if(!$header_image) {
		$header_image =  get_template_directory_uri().'/img/news-header-img@2x.png';
	}	


?>

	<div class="barttiersHeader" style="background: #F3F3F3 url(<?php echo $header_image; ?>); background-size: cover;">			
		<div class="imgCaption">
			<div class="col-xs-12 col-md-4 col-lg-3 padding0">
				<span class="title">
					<?php
						if (is_category()) :
							if(single_cat_title('',false) === 'News and Resources'){
								echo 'News';
							}else{
								single_cat_title();
							}
							

						elseif (is_tag()) :
							single_tag_title();

						elseif (is_author()) :
							/* Queue the first post, that way we know
							 * what author we're dealing with (if that is the case).
							 */
							the_post();
							printf(__('Author: %s', 'bootstrap-basic'), '<span class="vcard">' . get_the_author() . '</span>');
							/* Since we called the_post() above, we need to
							 * rewind the loop back to the beginning that way
							 * we can run the loop properly, in full.
							 */
							rewind_posts();

						elseif (is_day()) :
							printf(__('Day: %s', 'bootstrap-basic'), '<span>' . get_the_date() . '</span>');

						elseif (is_month()) :
							printf(__('Month: %s', 'bootstrap-basic'), '<span>' . get_the_date('F Y') . '</span>');

						elseif (is_year()) :
							printf(__('Year: %s', 'bootstrap-basic'), '<span>' . get_the_date('Y') . '</span>');

						elseif (is_tax('post_format', 'post-format-aside')) :
							_e('Asides', 'bootstrap-basic');

						elseif (is_tax('post_format', 'post-format-image')) :
							_e('Images', 'bootstrap-basic');

						elseif (is_tax('post_format', 'post-format-video')) :
							_e('Videos', 'bootstrap-basic');

						elseif (is_tax('post_format', 'post-format-quote')) :
							_e('Quotes', 'bootstrap-basic');

						elseif (is_tax('post_format', 'post-format-link')) :
							_e('Links', 'bootstrap-basic');

						else :
							_e('Archives', 'bootstrap-basic');

						endif;
						?> 			
					<div class="titleLine"></div>
				</span>
			</div>
			<div class="col-xs-7 col-md-8 col-lg-9 padding0 descriptionContainer">
				<span class="description">
						
				</span>	
			</div>
		</div>
	</div>
	<div class="container-fluid singleBarristerHeader contentFontProperties" id="main-column">
	<div class="col-sm-12" id="category-search-area">
		<div class="form-group">
			<form role="search" method="POST" action="">			
				
					
					<?php
					
					$practiceAreaTerms = get_terms( array(
							'taxonomy' => 'practice-area',
							'hide_empty' => false,
						)
					);
						
					?>
					<?php if (0) { // Requested by Jennifer Ukaegbu on 25.05.2017 (3. Please hide the filter option on this page and leave the search function please) ?>
				<select name="practice">
					<option value="" >Please Select</option>
					<?php 
                        $practice = (!empty($_POST['practice'])) ? $_POST['practice'] : '';
                    
						foreach ( $practiceAreaTerms as $term ) {
							$selected = ($practice == $term->term_id) ? 'selected' : '';
							echo '<option value="' . $term->term_id . '" '.$selected.' >'.$term->name .'</option>';
						}
					?>	
				</select>
					<?php } ?>
                <div class="clearfix padding5 visible-xs"></div>
				<div class="inner-addon left-addon inlineBlock">
				  <input type="text"  id="searchInput2" placeholder="Search" value="<?php echo $searchString; ?>" name="searchString" title="Search for:">
				  <i class="glyphicon glyphicon-search"></i>
				</div>
				
				<input type="submit" name="submit" class="btn archive-search-btn hidden-xs" value="Search">
				<div class="clearfix padding8 visible-xs"></div>
				
				
				<span class="follow-xs">Follow</span>
				<?php if(get_option('general_setting_twitter')){?>
				<a class="twitter-follow-button" href="<?php echo get_option('general_setting_twitter'); ?>" target="_blank">
					<img src="<?php  echo get_template_directory_uri();?>/img/twitter@2x.png" alt="follow-us-twitter">
				</a>
				<?php }
				
				if(get_option('general_setting_linkedin')){?>
				<a class="linkedin-follow-button" href="<?php echo get_option('general_setting_linkedin'); ?>" target="_blank">
					<img src="<?php  echo get_template_directory_uri();?>/img/linked@2x.png" alt="join-us-linkedin">
				</a>
				<?php }?>
				<hr class="visible-xs">
			</form>
		</div>
	</div>
	<div class="col-md-8 col-lg-9 padding0-xs" id="archive-content">
		<div class="col-sm-12 backWhite padding0-xs">
			<main id="main" class="site-main" role="main">
			
				<?php if(isset($categoryObj)){
				
						while ($categoryObj->have_posts()) {
							$categoryObj->the_post();

						?> 
						
						<h3>
							<a href="<?php echo get_permalink();?>">
								<?php echo get_the_title();?>
							</a>
						</h3>
				
						<div class="category-date">
							<?php echo get_the_date("j. M Y"); ?>
						</div>
						<div class="category-date-devider"></div>
						<div class="clearfix"></div><br/>
						<div class="category-excerpt">
							<?php the_excerpt();  ?>
						</div>
						
						<div class="clearfix"></div>
						<a href="<?php echo get_permalink();?>" class="btn btn-read-more"><?php _e('Read More', 'bootstrap-basic'); ?></a>
						<div class="category-listing-devider"></div>
						<?php 
							}
							
							
							wp_reset_postdata();
							
					}elseif(have_posts()) { ?> 

					<?php 
					/* Start the Loop */
					while (have_posts()) {
						the_post();

					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 *
					 *get_template_part('content', get_post_format());
					 */
					
					  ?> 
						
							<h3>
								<a href="<?php echo get_permalink();?>">
									<?php echo get_the_title();?>
								</a>
							</h3>
						
						
				
						<div class="category-date">
							<?php echo get_the_date("j. M Y"); ?>
						</div>
						<div class="category-date-devider"></div>
						<div class="clearfix"></div><br/>
						<div class="category-excerpt">
							<?php the_excerpt();  ?>
						</div>
						
						<div class="clearfix"></div>
						<a href="<?php echo get_permalink();?>" class="btn btn-read-more"><?php _e('Read More', 'bootstrap-basic'); ?></a>
						<div class="category-listing-devider"></div>
					
				<?php } //endwhile; ?> 

				

				<?php } else { ?> 

				<?php get_template_part('no-results', 'archive'); ?> 

				<?php } //endif; ?> 
				</div>
			</main>
			<?php 					
				if(!isset($categoryObj)){
        
					bootstrapBasicPagination();
				}
			?>
		</div>
		<div class="hidden-xs col-md-4 col-lg-3" id="archive-sidebar">
			<div class="col-sm-12 backWhite padding0">
				<div class="padding20">
					<h3 class="fontS20"><?php _e('Sign up for news and events', 'bootstrap-basic'); ?> </h3>
					<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?><br/>
				</div>	
			</div>	
			<div class="clearfix"></div><br/>	
			<?php get_template_part('content', 'rlc-sidebar'); ?>
		</div>
        <script>
            $(document).ready(function(){
                $( ".category-listing-devider" ).last().addClass('padding10').removeClass('category-listing-devider');

            });
        </script>
	</div>
<?php get_footer(); ?> 