<?php
/**
 * Template for displaying pages
 * 
 * @package bootstrap-basic
 */
get_header();
	wp_enqueue_style('main-style', get_template_directory_uri() . '/css/areas-of-expertise.css');
$header_img = get_template_directory_uri().'/img/Group.png';
if(get_the_post_thumbnail_url()){
	$header_img =	get_the_post_thumbnail_url();
}
 ?>
    <style>
        ul.children>li>a<? /*:after*/?> {
            background-image: url(<?php echo get_template_directory_uri();
            ?>/img/arrow-rot.png);
            
        }
        
        #main-column {
            background: #F3F3F3 url(<?php echo $header_img; ?>);
        }
        h1{
            font-size: 25px;
        }
        #TPLcontainer {
            padding: 25px 0 25px 15px;
        }
    </style>

    <div id="main-column" class="col-sm-12">
        <main id="area-of-expertise" class="site-main container-fluid" role="main">
            <div class="col-sm-12" id="TPLcontainer">
                <h1>
                    <?php echo get_the_title()?>
                </h1>
                <div class="redLine col-sm-1"></div>
                <div class="clearfix"></div>
                <div class="col-sm-5 col-md-4 col-lg-3" >
                    <div class="row" id="areas-of-expertise-content">
                        <?php
                            if (have_posts()) {
                                while (have_posts()) {
                                    the_post();
                                    the_content();
                                }
                            }  
                        ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                    <script>
                        $(document).ready(function() {
                            //$("#areas-of-expertise-list > li > a").after('<div class="col-sm-2 padding0"><hr/></div><div class="clearfix"></div>');
                        });

                    </script>

                    <div id="areas-of-expertise-list">
                        <?php 
                        wp_list_pages( array(
                            'child_of' => get_the_ID(),
                            'title_li' => '',
                            ))
                    ?>
                    </div>
                  
                    <div class="clearfix"></div><br>
                    <div class="padding20"></div>
            </div>
        </main>

    </div>

    <?php get_footer(); ?>
