<?php get_header(); ?> 
	<div class="barttiersHeader" style="background: #F3F3F3 url(<?php  echo get_template_directory_uri().'/img/news-header-img@2x.png'; ?>); background-size: cover; background-position: center;">
		<div class="imgCaption">
			<div class="col-sm-5 col-md-4 col-lg-3 padding0">
					<div class="title">
					<?php _e('Page not found', 'bootstrap-basic'); ?>
					<div class="titleLine"></div>
				</div>
			</div>
			<div class="col-sm-7 col-md-8 col-lg-9 padding0  hidden-xs descriptionContainer">
				<span class="description">
				
				</span>	
			</div>
		</div>
	</div>
	<div class="container-fluid padding0 singleBarristerHeader contentFontProperties" id="main-column">
		<div class="col-md-8 col-lg-9">
			<div class="col-sm-12 backWhite"><br/>
				<section class="error-404 not-found">
					<header class="page-header">
						<h1 class="page-title"><?php _e('Oops! That page can&rsquo;t be found.', 'bootstrap-basic'); ?></h1>
					</header><!-- .page-header -->

					<div class="page-content">
						<p><?php _e('It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'bootstrap-basic'); ?></p>

						<!--search form-->
						<form class="form-horizontal" method="get" action="<?php echo esc_url(home_url('/')); ?>" role="form">
							<div class="form-group">
								<div class="col-xs-10">
									<input type="text" name="s" value="<?php echo esc_attr(get_search_query()); ?>" placeholder="<?php echo esc_attr_x('Search &hellip;', 'placeholder', 'bootstrap-basic'); ?>" title="<?php echo esc_attr_x('Search &hellip;', 'label', 'bootstrap-basic'); ?>" class="form-control" />
								</div>
								<div class="col-xs-2">
									<button type="submit" class="btn btn-default"><?php _e('Search', 'bootstrap-basic'); ?></button>
								</div>
							</div>
						</form>

						<div class="row">
							<div class=" col-sm-6 col-md-3">
								<?php the_widget('WP_Widget_Recent_Posts'); ?> 
							</div>
							<div class=" col-sm-6 col-md-3">
								<div class="widget widget_categories">
									<h2 class="widgettitle"><?php _e('Most Used Categories', 'bootstrap-basic'); ?></h2>
									<ul>
										<?php
										wp_list_categories(array(
											'orderby' => 'count',
											'order' => 'DESC',
											'show_count' => 1,
											'title_li' => '',
											'number' => 10,
										));
										?> 
									</ul>
								</div><!-- .widget -->
							</div>
							<div class=" col-sm-6 col-md-3">
								<?php
								/* translators: %1$s: smiley */
								$archive_content = '<p>' . sprintf(__('Try looking in the monthly archives. %1$s', 'bootstrap-basic'), convert_smilies(':)')) . '</p>';
								the_widget('WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content");
								?> 
							</div>
							<div class=" col-sm-6 col-md-3">
								<?php the_widget('WP_Widget_Tag_Cloud'); ?> 
							</div>
						</div>
					</div><!-- .page-content -->
				</section><!-- .error-404 -->
	
			</div>
		</div>
		<div class="hidden-xs col-md-4 col-lg-3">
			<div class="col-sm-12 backWhite">
				<h3><?php _e('Sign up for news and events', 'bootstrap-basic'); ?> </h3>
				<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?><br/>
			</div>	
			<div class="clearfix"></div><br/>	
			<div class="col-sm-12 backWhite"> 
				<h3 class="view-our-barristers-sidebar"><a href="/barristers"><?php _e('View our Barristers', 'bootstrap-basic'); ?></a></h3>
			</div>
			<div class="clearfix"></div><br/>
			<div class="col-sm-12 backWhite"> 
				<h3 class=""><?php _e('Contact us', 'bootstrap-basic'); ?></h3>
				<br/>
				<?php 
					if(get_option('setting_email')){
						echo '<div class="col-sm-12 padding0"><img src="' . get_template_directory_uri() . '/img/email icon.png" class="padding5">' . get_option('setting_email').'</div>';
					}
					
					if(get_option('setting_phone')){
						echo '<div class="col-sm-12 padding0"><img src="' . get_template_directory_uri() . '/img/call icon.png" class="padding5">' . get_option('setting_phone').'</div>';
					} 
				?>
				<div class="clearfix"></div><br/>
				<a href="/contact-us/" class="btn btn-enquire">Enquire</a>
				<div class="clearfix"></div><br/>

				<script>
					$(document).ready(function(){
						$( ".category-listing-devider" ).last().addClass('padding10').removeClass('category-listing-devider');
					});
				</script>
			</div>
		</div>
	</div>

<?php get_footer(); ?> 