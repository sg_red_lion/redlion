<?php 
/**
*
* Displaying Archive management
* @package bootstrap-basic
* 
*/ 
	//error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	get_header(); 



	$staff_args = array(
		'post_type' => 'management',
		'post_status' => 'publish',
		'posts_per_page' => '1000',
		'orderby' => 'display_section',
		'order' => 'ASC',
	); 	
	$staff = new WP_Query( $staff_args ); 

	//$posts = $barrister_queens->posts;
	// echo $barrister_queens->request;
	// var_dump($posts);

?>
<style>
	.placeFiller{ height:240px !important;}

	.barttiersHeader .title, .barttiersHeader .description{
		height: 120px;
		padding: 5% 41px;
	}
	
	.imageHolder h2{
		color: black;
	}
	.paddingTB35{padding: 35px 0;}
	.popover.in {
	    opacity: 1;
	    filter:alpha(opacity=100);
	    max-width: 550px; 
	    z-index: 999999999999;
		padding:15px 5px;
		background-color: #fff;
		line-height:25px;
	  
	}
	.popover {
		border: 1px solid #c61912;
		margin-left:70px;
	}
	
	.popover.bottom>.arrow {
		border-bottom-color: #c61912;
		margin-left:-80px;
	}
	
	.staff-contact{
		cursor:pointer;
		width:70px;
	}
	.staff-contact-holder{
		DISPLAY: INLINE-BLOCK;
	}
	@media screen and (min-width: 1599px) {
		.placeFiller{ height: 360px !important;}
	}
	
	@media (max-width: 767px){
		.imgCaption {
			margin-top: -140px;
		}
		
		.barttiersHeader, .barttiersHeader img{
			height: 220px;
		}	
		.barttiersHeader .title, .barttiersHeader .description{
			height: 120px;
			padding: 9% 41px;
		}
		.staff-contact{
			width:70%;
			padding:10px;
			display: block;
			margin-right: auto;
			margin-left: auto;
		}
		.staff-contact-holder{
			margin: 0 0px 25px 0px;
			background: white;
			padding: 0;
			text-align: center;
			height: 183px;
			border-top: 1px solid gray;
			border-bottom: 1px solid gray;
			width: 50%;
			float: left;
		}
	
		.borderLeft{
			border-left: 1px solid gray;
		}
		
		.borderRight{
			border-right: 1px solid gray;
		}
		.imageHolder{
			position: relative; 
		}
		.staff-info-xs { 
		    background: rgba( 0, 0, 0, .6);
			position: absolute;
			top: 215px;
			left: 0;
			width: 300px;
			color: white !important;
			padding-left: 13px;
			padding-top: 5px;
			margin-left: 15px;

		}
		.staff-name-xs{
			font-size:24px;
		}
	}

 </style>
 <script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({html:true}); 
});
</script>

<div class="barttiersHeader" style="background: #F3F3F3 url(<?php echo get_template_directory_uri().'/img/management@2x.png'; ?>); background-size: cover;">	
	<div class="placeFiller"></div>	
	<div class="imgCaption">
		<div class="col-sm-4 padding0">
			<span class="title">
				<?php post_type_archive_title() ?>
				<div class="titleLine"></div>
			</span>
		</div>
		<div class="col-sm-8 padding0  hidden-xs descriptionContainer">
			<span class="description"></span>	
		</div>
	</div>
</div>
<div class="clearfix"></div>

	<div class="container-fluid padding0 singleBarristerHeader contentFontProperties" id="main-column">
		<main id="main" class="site-main" role="main">
			<div class="col-sm-8 col-md-9">
				<div class="col-md-12">
					<?php 
						if(($staff->have_posts())) {
							while ( $staff->have_posts() ) : $staff->the_post(); 
					?>
					
							<div class="col-sm-12 backWhite paddingTB35">
							<div class="paddingTB30">
								<div class="col-sm-4 imageHolder">
									<div class="redLine col-sm-6 padding5 hidden-xs"></div>
									<h2><?php the_title(); ?></h2>
									<span class="text-muted"><?php the_field('position'); ?></span>
									<div class="">
										<hr class="col-sm-2" />
									</div>
									<img class="img-responsive" src="<?php the_post_thumbnail_url();?>" alt="" />
									
									<div class="staff-info-xs visible-xs">
										<div class="staff-name-xs"><?php the_title(); ?>
									</div>
										<?php the_field('position'); ?>
									</div>
								</div>
								<div class="col-sm-8">
									<div class="padding20 visible-xs"></div>
									<?php the_content(); ?>
									<div class="clearfix"></div>
									<div class="staff-contact-holder borderLeft">
										<img class="alignnone size-full wp-image-344 padding5 staff-contact" src="<?php echo get_template_directory_uri(); ?>/img/call-icon@2x.png" alt="" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="<strong>Telephone:</strong> <?php the_field('phone'); ?>" />
										<div class="visible-xs h4 text-center">
											Call Profile
										</div>
									</div>
									<div class="staff-contact-holder borderRight borderLeft">
										<img class="alignnone size-full wp-image-345 padding5 staff-contact" src="<?php echo get_template_directory_uri(); ?>/img/email-icon@2x.png" alt="" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="
										<strong>Direct email:</strong> <?php the_field('direct_email'); ?> <br>
										<strong>Team email</strong>: <?php the_field('team_email'); ?><br>
										<strong>CJSM email</strong>:<?php the_field('cjsm_email'); ?>" />
										<div class="visible-xs h4 text-center">
											Email Profile
										</div>
									</div>
								</div>
								&nbsp;

							</div>
							<div class="clearfix"></div><br/>
						</div>
						<div class="clearfix"></div><br/>
					
					<?php
							endwhile; 
						}
					?>
							
					<div class="clearfix"></div><br/>
					
						

						<?php wp_reset_postdata(); ?>
						
				</div>
			</div>
			<div class="hidden-xs col-sm-4 col-md-3">
				<div class="col-sm-12 backWhite"> 
					<h3 class=""><?php _e('In this section', 'bootstrap-basic'); ?></h3>
					<?php 
						wp_list_pages( array(
						'child_of' => get_the_ID(),
						'title_li' => '',
						))
					?> 
				</div>
				<div class="clearfix"></div><br/>
				
				<div class="col-sm-12 backWhite"> 
					<h3 class=""><?php _e('Contact us', 'bootstrap-basic'); ?></h3>
					<br/>
					<?php 
						if(get_option('setting_email')){
                            echo '<div class="col-sm-12 padding0"><img src="' . get_template_directory_uri() . '/img/email icon@2x.png" class="contactSidebarIcons"><a class="black-text" href="mailto:'.get_option('setting_email').'">' . get_option('setting_email').'</a></div>';
                        }
                    
						if(get_option('setting_phone')){
                            echo '<div class="col-sm-12 padding0"><img src="' . get_template_directory_uri() . '/img/call icon@2x.png" class="contactSidebarIcons"><a class="black-text" href="tel:'.get_option('setting_phone').'">' . get_option('setting_phone').'</a></div>';
                        }
					?>
					<div class="clearfix"></div><br/>
					<a href="/contact-us/contact-form-2/" class="btn btn-enquire">Enquire</a>
					<div class="clearfix"></div><br/>
				</div>
			</div>
		</main>
	</div>



	
	<?php get_footer(); ?> 