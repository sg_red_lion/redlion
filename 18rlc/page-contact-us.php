<?php
/**
 * Template for displaying pages
 * 
 * @package bootstrap-basic
 */
get_header();

?>
    <style>
        .locationContent {
            padding: 25px;
        }
        
        
        
        .locationTitle {
            color: #C61912;
            font-size: 18pt;
            line-height: 2;
            font-family: 'Domine';
        }
        
        #mapTwo,
        #mapOne {
            width: 100%;
            height: 280px
        }
        
        .call-to-action-contact {
            padding-left: 125px;
            float: right;
        }
        
        .address-column {
            padding: 0;
        }
        
        .btn-enquire {
            float: right;
        }
        
        @media (max-width:767px) {
           
            strong{
                font-family: 'Domine';
                border-color: #4D4D4D;
                font-size: 16pt;
            }
            .call-to-action-contact {
                padding: 0;
                float: left;
                padding: 0;
                width: 60%;
            }
            .address-column {
                padding-left: 15px;
            }
            .btn-enquire {
                float: none;
            }
           .locationContent {
                padding: 0 0 20px 0;
            }
           
            #alternatively-enquiry .btn-enquire{
                margin-top: 20px;
                margin-bottom: 30px;
            }
            main{
                font-family: 'Open Sans', sans-serif;
                color: #6A6A6A;
                font-size: 12pt;
                padding-top: 10px;
            }
        }

    </style>
    <?php 
        $terms = get_the_terms( get_the_ID(), 'category');
        $header_image = '';

        if(!empty($terms)){
            $term = array_pop($terms);
            $header_image = get_field('header_image', $term );
        } 

        if(!$header_image) {
            $header_image =  get_template_directory_uri().'/img/backgroundGetInTouch.png';
        }
    ?>
    <div class="barttiersHeader visible-xs" style="background: #F3F3F3 url(<?php echo $header_image; ?>); background-size: cover;">			
		<div class="imgCaption">
			<div class="col-xs-12 col-md-4 col-lg-3 padding0">
				<span class="title">
					<?php
                        the_title();
                    ?> 			
					<div class="titleLine"></div>
				</span>
			</div>
			<div class="col-xs-7 col-md-8 col-lg-9 padding0 descriptionContainer">
				<span class="description">
						
				</span>	
			</div>
		</div>
	</div>
    
    <main role="main">
        <?php the_content(); ?>
        <div id="mapOne" class="hidden-xs"></div>
        <div class="container-fluid">
            <div class="locationContent">
                <span class="locationTitle">London</span>
                <div class="hidden-xs">
                    <a href="/contact-us/contact-form-2/" class="btn btn-enquire">Contact Us</a>
                    <div class="col-sm-5 call-to-action-contact">
                        If you would like to send us an enquiry, please click the link on the right to complete our online enquiry form. 
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="locationDivider"></div>
                <div class="col-sm-4 padding0"><br>
                    <?php the_field('london_address'); ?>
                </div>
                <div class="col-sm-3 padding0 hidden-xs"><br>
                    <strong>Opening Hours:</strong><br><br> Monday - Friday 8.45am - 6pm
                </div>
                <div class="col-sm-5 padding0 hidden-xs"><br>
                    <strong>Emergency Assistance:</strong><br><br> We provide an out of hours service in cases of emergency.<br> Please contact us on 07710 077 419
                </div>
            </div>
        </div>

        <div id="mapTwo" class="hidden-xs"></div>

        <div class="container-fluid">
            <div class="locationContent">
                <span class="locationTitle">East Anglia</span>
                <div class="hidden-xs">
                    <a href="/contact-us/contact-form-2/" class="btn btn-enquire">Contact Us</a>
                    <div class="col-sm-5 call-to-action-contact">
                        If you would like to send us an enquiry, please click the link on the right to complete our online enquiry form. 
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="locationDivider"></div>
                <div class="col-sm-4 padding0"><br>
                    <?php the_field('east_anglia_address'); ?>
                </div>
                <div class="locationDivider visible-xs"></div>
                <div class="col-sm-3 padding0"><br>
                    <strong>Opening Hours:</strong><br><br> Monday - Friday 8.45am - 6pm
                </div>
                <div class="col-sm-5 padding0"><br>
                    <strong>Emergency Assistance:</strong><br><br> We provide an out of hours service in cases of emergency<br> Please contact us on 07710 077 419
                </div>
                <div class="clearfix"></div><br>
                <div class="locationDivider"></div>
            </div>
            
            <div class="visible-xs" id="alternatively-enquiry">
                Alternatively, you can comlete our online enquiry form
                <div class="clearfix"></div>
                <a href="/contact-us/contact-form-2/" class="btn btn-enquire">Enquiry Form</a>
                <div class="clearfix"></div>
                <div class="locationDivider"></div>
                <div class="clearfix"></div><br><br>
            </div>
        </div>
    </main>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA07IdV-neqeWKo3aQuds9fnIA2A9ntF5E&callback=initMap" async defer></script>
    <script type="text/javascript">
        var map;

        function initMap() {
            var myLatLngOne = {
                lat: 51.514856,
                lng: -0.108934
            };

            mapOne = new google.maps.Map(document.getElementById('mapOne'), {
                center: myLatLngOne,
                zoom: 16,
                scrollwheel: false,
            });

            var markerOne = new google.maps.Marker({
                position: myLatLngOne,
                map: mapOne
            });
            markerOne.setMap(mapOne);

            var myLatLngTwo = {
                lat: 51.729465,
                lng: 0.468109
            };
            mapTwo = new google.maps.Map(document.getElementById('mapTwo'), {
                center: myLatLngTwo,
                zoom: 17,
                scrollwheel: false,
            });

            var markerTwo = new google.maps.Marker({
                position: myLatLngTwo,
                map: mapTwo
            });

            markerTwo.setMap(mapTwo);
        }

    </script>
    <?php get_footer(); ?>
