<?php
/**
 * Template for aside post format
 * 
 * @package bootstrap-basic
 */
?>

			
			<div class="col-sm-12 backWhite"> 
				<h3 class="view-our-barristers-sidebar"><a href="/barristers"><?php _e('View our Barristers', 'bootstrap-basic'); ?></a></h3>
			</div>
			<div class="clearfix"></div><br/>
			
			<div class="col-sm-12 backWhite"> 
				<h3 class="contact-us-sidebar"><?php _e('Contact us', 'bootstrap-basic'); ?></h3>
				
				<br/>
				<?php 
					if(get_option('setting_email')){
						echo '<div class="col-sm-12 padding0"><img src="' . get_template_directory_uri() . '/img/email icon@2x.png" class="contactSidebarIcons"><a class="black-text" href="mailto:'.get_option('setting_email').'">' . get_option('setting_email').'</a></div>';
					}
					
					if(get_option('setting_phone')){
                        echo '<div class="col-sm-12 padding0"><img src="' . get_template_directory_uri() . '/img/call icon@2x.png" class="contactSidebarIcons"><a class="black-text" href="tel:'.get_option('setting_phone').'">' . get_option('setting_phone').'</a></div>';
                    }
				?>
				<div class="clearfix"></div><br/>
				<a href="/contact-us/contact-form-2/" class="btn btn-enquire">Enquire</a>
				<div class="clearfix"></div><br/>
			</div>
			