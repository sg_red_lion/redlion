<?php
/**
*
* Displaying Feature picture as background 
* @package bootstrap-basic
* Template Name: Feature picture as background 
*
*/ 
get_header();

//$parentId = wp_get_post_parent_id(get_the_ID()); 

$defaultImg = get_template_directory_uri().'/img/our-people-BG.png';
$postThumbnailUrl = get_the_post_thumbnail_url(get_the_ID());

?>
    <script>
        $(document).ready(function(){
            $('.removeArrs').find('div, ul, li').removeAttr('id').removeAttr('class'); 
        });
    </script>
    
    <style>
        #main-column{
            min-height: 600px;
        }
        h1 {
            font-size: 25px;
        }
        #TPLcontainer {
            color: white;
            padding: 25px 0 25px 50px;
            cursor:default;
            pointer:default;
        }
        
        #childPages ul{
            list-style: none;
            margin-left: -40px;
            font-size: 18px;
        }
        #childPages ul ul li{
            padding-left: 20px;
            font-size: 16px;
        }
        #childPages ul ul li:last-child {
            border-bottom: none; 
        }
        #childPages li a {
            color: white;
        }
        
        #childPages li {
            color: white;
            border-top: 1px dotted white;
            line-height: 45px;
        }
        
        #childPages li:last-child {
            color: white;
            border-bottom: 1px dotted white;
        }
        
        #childPages li a:after {
            background-image: url(<?php echo get_template_directory_uri().'/img/arrow.png'; ?>);
            background-size: 10px 20px;
            background-repeat: no-repeat;
            background-position: center;
            content: "";
            display: inline-block;
            width: 10px;
            height: 45px;
            float: right;
            -webkit-transform: rotate(180deg);
            -moz-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            -o-transform: rotate(180deg);
            transform: rotate(180deg);
        }
        .children li{
            border: none;
            font-size: 16px;
            
        }
        @media (max-width: 767px) {
            #TPLcontainer{
               padding: 0 15px;
            }
        }
        
    </style>



    <div class="container-fluid" id="main-column" style="background: #F3F3F3 url(<?php echo ($postThumbnailUrl ? $postThumbnailUrl : $defaultImg); ?>); background-size: cover;">
        <main id="main" class="site-main" role="main">
            <div class="row">
                <div class="col-sm-5 col-md-4 col-lg-3" id="TPLcontainer">
                    <h1>
                        <?php echo get_the_title()?>
                    </h1>
                    <div class="col-sm-3 redLine marginB10"></div>
                    <?php 
                    while (have_posts()) {
                        the_post();

                        get_template_part('content', 'page');

                        echo "\n\n";

                        // If comments are open or we have at least one comment, load up the comment template
                        if (comments_open() || '0' != get_comments_number()) {
                            comments_template();
                        }

                        echo "\n\n";

                    } //endwhile;
                    ?>


                    <div id="childPages" class="removeArrs">
                        <?php 
//						wp_list_pages( array(
//						'child_of' => get_the_ID(),
//						'title_li' => '',
//						))
                        
                        wp_nav_menu( array(
                          'theme_location' => 'primary',
                          'sub_menu'       => true,
                          'walker' =>'',
                            'items_wrap' => '<ul id="childPages">%3$s</ul>'
                        ) );
					?>
                    </div>

                </div>
            </div>
        </main>
    </div>

    <?php get_footer(); ?>
