<?php
/**
 * The template for displaying search results.
 * 
 * @package bootstrap-basic
 */

get_header();

error_reporting(E_ALL & ~E_NOTICE);
?>
    <div class="barttiersHeader" style="background: #F3F3F3 url(<?php  echo get_template_directory_uri().'/img/news-header-img@2x.png'; ?>); background-size: cover;">
        <div class="imgCaption">
            <div class="col-sm-5 col-md-4 col-lg-3 padding0">
                <div class="title">
                    Search Results
                    <div class="titleLine"></div>
                </div>
            </div>
            <div class="col-sm-7 col-md-8 col-lg-9 padding0  hidden-xs descriptionContainer">
                <span class="description">
			
			</span>
            </div>
        </div>
    </div>

    <style>
        ul.red-lion-pagination li a,
        ul.red-lion-pagination li.active span.current {
            background: white;
            margin-right: 5px;
            height: 38px;
        }
        
        ul.red-lion-pagination li a.next,
        ul.red-lion-pagination li a.prev {
            background: #c61912;
            color: white;
        }
        
        select,
        option {
            text-align-last: center;
        }

    </style>
    <script>
        $(document).ready(function() {
            $('[data-toggle="popover"]').popover({
                html: true
            });
        });

    </script>


    <?php  wp_enqueue_style('management-and-staff', get_template_directory_uri() . '/css/management_and_staff.css', array()); ?>
    <main id="main" class="site-main" role="main">
        <div class="container-fluid padding0 singleBarristerHeader contentFontProperties" id="main-column">
            <div class="col-md-8 col-lg-9 " id="single-content">
                <div class="col-sm-12 backWhite padding0-xs"><br/>
                    <div class="inner-addon left-addon visible-xs">
                        <i class="glyphicon glyphicon-search"></i>
                        <input type="text" class="form-control input-sm menuSearch-xs" placeholder="<?php echo esc_attr_x('Search &hellip;', 'placeholder', 'bootstrap-basic'); ?>" value="<?php echo esc_attr(get_search_query()); ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label', 'bootstrap-basic'); ?>">
                    </div>
                    <div class="clearfix"></div>
                    <div class="barristerListigDevider-xs"></div>
                    <div class="col-sm-12 padding0-xs" id="category-search-area">

                        <div class="col-sm-4 padding0 hidden-xs">
                            <span class="page-title">
                            <?php printf(__('Results for: %s', 'bootstrap-basic'), '<strong>' . get_search_query() . '</strong>'); ?>
                        </span>
                        </div>
                        <div class="col-xs-12 col-sm-8 padding0 text-right">
                            <form action="" method="GET">
                                Sort by
                                <select name="orderby">
                                   <option value="">Relevence</option>
                                   <option value="date" <?php if($_GET['orderby'] == 'date') echo 'selected';?>>Date</option>
                                   <option value="author" <?php if($_GET['orderby'] == 'author') echo 'selected';?>>Author</option>
                                   <option value="title" <?php if($_GET['orderby'] == 'title') echo 'selected';?>>Title</option>
                                </select>
                                <select name="post_type" class="marginR20">
                                    <option value="">All</option>
                                    <?php
                                            $args = array(
                                               'public'   => true,
                                                '_builtin' => false
                                            );
                                         $post_types = get_post_types( $args, 'objects' );

                                            foreach ($post_types  as $post_type ) {
                                                if($post_type->name ==  $_GET['post_type']){
                                                    $selected = 'selected';
                                                }else{
                                                    $selected = '';
                                                }
                                            echo '<option value="' . $post_type->name . '" ' . $selected . '>' . $post_type->label . '</option>';

                                    }
                                    ?>
                               </select> 
                              
                          
                                <input type="hidden" name="s" value="<?php echo  $_GET['s']?>">
                                <input type="submit" value="Advanced Search" class="btn archive-search-btn hidden-xs">
                            </form>
                        </div>
                        <div id="results_found" class="visible-xs">
                            <div class="clearfix padding10"></div>
                            <?php 
                                global $wp_query;
                                echo $wp_query->found_posts.' results found.';
                            ?>
                        </div>
                        <div class="barristerListigDevider-xs"></div><br>
                    </div><br/><br/>
                    <?php if (have_posts()) { ?>
                    <?php 
				// start the loop
				while (have_posts()) {
					
						the_post();
						/* Include the Post-Format-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Format name) and that will be used instead.
						*/
					
						
						?>
                    <div class="col-sm-12">
                        <div class="search-listing-devider"></div>
                    </div><br/>
                    <div class="col-sm-3 paddingR0">
                        <span class="h4 searchResultPostType"><?php // var_dump();
								if (get_post_type(get_the_ID()) == 'barrister'){
									echo 'Barristers';
								}elseif(get_the_category()[0]->name == 'News and Resources'){
									echo 'News';
								}elseif(get_post_type(get_the_ID()) == 'management'){
									echo 'People';
								}elseif(get_post_type(get_the_ID()) == 'our-seminars'){
									echo 'Events';
								}else{
									if(get_the_category()[0]->name){
										echo get_the_category()[0]->name;
									}else{
										echo get_post_type(get_the_ID());
									}
								}
								?>
							</span	>
							<div class="search-title-devider"></div>
							<h4>
								<a href="<?php the_permalink();?>" class="searchResultTitle">
									<?php the_title(); ?> 
								</a>
							</h4>
							<br/>
							<?php if ('post' == get_post_type() or 'Seminars' == get_post_type()) { ?> 
                                <?php echo get_the_date("j. M Y"); ?>								
							<?php } //endif; ?> 
							<?php the_field('position'); ?>
						</div>
						
						<div class="col-sm-9">
						
							<?php if(get_post_type(get_the_ID()) == 'management' or get_post_type(get_the_ID()) == 'barrister'){ ?>
                                  <div class="col-sm-8"> 
                                       <?php the_excerpt(); ?> 
                                       <?php if(get_field('phone')){ ?>
                                            <a href="tel:<?php the_field('phone'); ?>" class="staff-contact-holder borderLeft hidden-xs">
                                                <img class="alignnone size-full wp-image-344 padding5 staff-contact" src="<?php echo get_template_directory_uri(); ?>/img/call-icon@2x.png" alt="" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="<strong>Telephone:</strong> <?php the_field('phone'); ?>" />
                                            </a>
                                        <?php } ?>
                                        <?php if(get_field('direct_email') or get_field('team_email') or get_field('cjsm_email')){ ?>
                                            <div class="staff-contact-holder borderRight borderLeft hidden-xs">
                                                <img class="alignnone size-full wp-image-345 padding5 staff-contact" src="<?php echo get_template_directory_uri(); ?>/img/email-icon@2x.png" alt="" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="
                                                <strong>Direct email:</strong> <?php the_field('direct_email'); ?> <br>
                                                <strong>Team email</strong>: <?php the_field('team_email'); ?><br>
                                                <strong>CJSM email</strong>:<?php the_field('cjsm_email'); ?>" />
                                            </div>
                                    <?php } ?>
                                    <?php if(get_field('email') ){ ?>
                                            <a href="mailto:<?php the_field('email'); ?>" class="staff-contact-holder borderRight borderLeft hidden-xs">
                                                <img class="alignnone size-full wp-image-345 padding5 staff-contact" src="<?php echo get_template_directory_uri(); ?>/img/email-icon@2x.png" alt="" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="
                                                <strong>Email:</strong> <?php the_field('email'); ?> " />
                                            </a>
                                    <?php } ?>
                                </div>
                                <a href="<?php the_permalink();?>">
                                    <div class="col-sm-4 paddingR0">
                                        <img src="<?php if(get_the_post_thumbnail_url()){the_post_thumbnail_url();}else{ echo get_template_directory_uri().'/img/red-pic@2x.png';};?>" alt="<?php the_title()?>" class="img-responsive">
                                    </div>
                                </a>
                            <?php }else{ 
                                   the_excerpt(); 
                            
							 } ?> 
							
						</div>
						
						
						<div class="clearfix"></div>
						
						
						
						<?php
					
				}// end while
				
				
				?> 
				<?php } else { ?> 
				<?php get_template_part('no-results', 'search'); ?>
				<?php } // endif; ?> <br/>
			</div><div class="clearfix"></div><br/>
			
			<?php bootstrapBasicPagination();?><br/>
		</div>
		<div class="hidden-xs col-md-4 col-lg-3" id="search-sidebar"><br>
			<div class="col-sm-12 backWhite">
				<h3 class="fontS20"><?php _e('Sign up for news and events', 'bootstrap-basic'); ?> </h3>
				<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?><br/>
			</div>	
			<div class="clearfix"></div><br/>	
			<?php get_template_part('content', 'rlc-sidebar'); ?>
			<div class="clearfix"></div><br/>
				<script>
					$(document).ready(function(){
						$( ".category-listing-devider" ).last().addClass('padding20 asdfg').removeClass('category-listing-devider');
					});
				</script>
		</div>
	</div>
</main>

<?php get_footer(); ?>
