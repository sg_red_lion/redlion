<?php
/**
 * Template for displaying page Management and Staff
 * 
 * @package bootstrap-basic
 */
get_header();


 $parentId = wp_get_post_parent_id(get_the_ID()); 
 
	$door_tenants_args = array(
		'post_type' => 'barrister',
		'post_status' => 'publish',
		'posts_per_page' => '1000',
        'meta_key' =>  'priority_order',
		'orderby' => 'meta_value_num',
        'order' => 'ASC',
		'meta_query'       => array(
			//comparison between the inner meta fields conditionals
			'relation'    => 'AND',
			//meta field condition one
			'honorary_clause' =>array(
				'key'          => 'honorary_door_tenant',
				'value'        => 'yes',
			)	
		),

	);
	$door_tenants = new WP_Query( $door_tenants_args ); 
 ?> 

<style>
    .barrister-name-link, 
    .barrister-name-link:hover, 
    .barrister-name-link:focus,
    .barrister-name-link:visited {

        cursor: default;
    }
    
   
</style>
<div class="barttiersHeader" style="background: #F3F3F3 url(<?php echo get_the_post_thumbnail_url(); ?>); background-size: cover;">	
	<div class="imgCaption">
		<div class="col-xs-12 col-sm-4 padding0">
			<span class="title">
				<?php the_title(); ?>
				<div class="titleLine"></div>
			</span>
		</div>
		<div class="col-xs-6 col-sm-8 padding0 descriptionContainer">
			<span class="description">
				<span class="hidden-xs">
					<?php the_content(); ?>
				</span>	
			</span>	
		</div>
	</div>
</div>
<div class="clearfix"></div><br/>

<div class="contariner-fluid content-area" id="main-column">
        <div class="col-sm-12 padding0">
            <div class="col-sm-12 backWhite paddingTB35">
                <div class="col-xs-12 seniorityTitle padding0">
                    <div class="col-sm-2 padding0 ">
                        <div class="redLine col-sm-6 padding5"></div>
                        <div class="clearfix"></div>
                        <?php _e('Door Tenants', 'bootstrap-basic'); ?>
                    </div>
                </div>
                <div class="padding15 col-sm-12"></div>
                <div class="col-sm-12 col-md-2"></div>
                <div class="col-sm-12 col-md-8 padding0">
                    <div class="col-sm-6 padding0">
                        <div class="col-xs-12 padding0">
                            <div class="co-xs-12 titleDevider-xs"></div>
                            <div class="nameTitle padding0 col-xs-8">
                                <?php _e("Name", 'bootstrap-basic'); ?>
                                <div class="clearfix"></div>
                                <div class="underlineNameTitle"></div>
                                <div class="clearfix"></div>
                            </div>
                                <?php /*<div class="silkTitle col-xs-2">
                                <?php _e("Silk", 'bootstrap-basic'); ?>
                            </div>
                            <div class="callTitle col-xs-2">
                                <?php _e("Call", 'bootstrap-basic'); ?>
                            </div>*/?>
                        </div>
                        <div class="col-xs-12 padding5"></div>

                    <?php 
                        if(($door_tenants->have_posts())) {
                            $doorTenantsNum = $door_tenants->post_count;
                            $doorTenantsHalf = (int)($doorTenantsNum / 2);
                            $i = 1;

                        while ( $door_tenants->have_posts() ) : $door_tenants->the_post(); 

                            if ($i <= $doorTenantsHalf) {
                                ?>								
                                <div class="col-xs-8 padding0">
                                    <?php /*<a class="barrister-name-link"  data-name="<?php the_title();?>" data-image="<?php echo get_the_post_thumbnail_url();?>" href="<?php echo get_permalink();?>"><?php the_title(); ?></a> */?>
                                    <span class="barrister-name-link" data-name="<?php the_title();?>"  data-image="<?php echo get_the_post_thumbnail_url();?>"><?php the_title();?></span>
                                     <?php if(get_field('listing_extra_text')){ echo '<small class="barrister_listing_extra_text">('.get_field('listing_extra_text').')</small>';  } ?>
                                </div>
                                    <?php /*<div class="col-xs-2 silkValue">
                                    <?php the_field('Silk'); ?>
                                </div>
                                <div class="col-xs-2 callValue">
                                    <?php the_field('Call'); ?>
                                </div> */ ?>
                        <?php
                            }else{ 
                                if($i == $doorTenantsHalf + 1 ){ ?>
                    </div>
                    <div class="col-sm-6 padding0">
                        <div class="col-sm-12 padding0 hidden-xs"> 
                            <div class="co-sm-12"></div>
                            <div class="nameTitle padding0 col-sm-8">
                                <?php _e("Name", 'bootstrap-basic'); ?>

                                <div class="clearfix"></div>
                                <div class="underlineNameTitle"></div>
                                <div class="clearfix"></div>
                            </div>
                                <?php /*<div class="silkTitle col-sm-2">
                                <?php _e("Silk", 'bootstrap-basic'); ?>
                            </div>
                            <div class="callTitle col-sm-2">
                                <?php _e("Call", 'bootstrap-basic'); ?>
                            </div>*/?>
                        </div>
                        <div class="col-xs-12 padding5 hidden-xs"></div>
                        <?php } ?>

                            <div class="col-xs-8 padding0">
                                <?php /*<a class="barrister-name-link"  data-name="<?php the_title();?>" data-image="<?php echo get_the_post_thumbnail_url();?>" href="<?php echo get_permalink();?>"><?php the_title();?></a>*/ ?>

                                <span class="barrister-name-link"  data-name="<?php the_title();?>"  data-image="<?php echo get_the_post_thumbnail_url();?>" ><?php the_title();?></span>
                                 <?php if(get_field('listing_extra_text')){ echo '<small class="barrister_listing_extra_text">('.get_field('listing_extra_text').')</small>';  } ?>
                            </div>
                            <?php /*<div class="col-xs-2 silkValue">
                                <?php the_field('Silk'); ?>
                            </div>
                            <div class="col-xs-2 callValue">
                                <?php the_field('Call'); ?>
                            </div>
                            <?php	*/
                            }
                            $i++;

                            endwhile; 
                            }else{
                        ?>
                    </div>

                    <?php } ?>
                </div>
                <div class="col-sm-12 col-md-2"></div>

            </div>
        </div>

	<div class="clearfix"></div>
		
    <div class="col-sm-12 col-md-2"></div>
	<div class="col-sm-12 col-md-7 padding0-xs">
	<?php  if(!$door_tenants->have_posts()) { _e("No door tenants found", 'bootstrap-basic'); } ?>
		<?php /*<img src="<?php echo get_template_directory_uri();?>/img/red rectangle .png" class="pull-right hidden-sm hidden-xs" alt="red rectangle"> */ ?>
	</div>
    <div class="col-sm-12 col-md-3"></div>
	<?php wp_reset_postdata(); ?>
	<div class="clearfix"></div><br/>
</div><!--End .contariner-fluid .content-area-->				

<script>
   function checkOffset() {
        if($('#hoveredBaristerData').offset().top + $('#hoveredBaristerData').height() >= $('#footer').offset().top - 10){
            $('#hoveredBaristerData').css('position', 'absolute').css('bottom','0');
        } 
        if($(document).scrollTop() + window.innerHeight < $('#footer').offset().top){
            $('#hoveredBaristerData').css('position', 'fixed').css('bottom','50px'); // restore when you scroll up
        }

    }

    $(document).scroll(function() {
        checkOffset();
    });

    $(document).ready(function(){
       /* $( ".barrister-name-link" )
          .mouseover(function() {
                checkOffset();
                var imageSrc = $(this).data("image");
                var name =  $(this).data("name");
                if(!imageSrc){
                    imageSrc = '<?php echo get_template_directory_uri();?>/img/red pic.png';
                }
                $('#hoveredBaristerImage').attr("src", imageSrc);
                $('#hoveredBaristerName').text(name);
                $( "#hoveredBaristerData").css('visibility','visible');
          })
          .mouseout(function() {
            $( "#hoveredBaristerData").css('visibility','hidden');
          });*/
    });
    
</script>
<div class="clearfix"></div>
<div id="hoveredBaristerDataParrent">
    <div id="hoveredBaristerData">
        <img id="hoveredBaristerImage" src="" alt="barrister name"/>
        <div id="hoveredBaristerName"></div>
    </div>
</div>
<div class="clearfix"></div><br/>
<?php get_footer(); ?> 