<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if(get_the_ID() != 321){ // not "Case Enquiry Questionnaires" ?>
		<header class="entry-header">
			<h1 class="entry-title h3">
			<?php
//				if(get_the_ID() === 1577){
//					echo 'News';
//				}else{
					if(!is_page()){
						the_title();
					}
//					
//				}
			 ?>
			 </h1>
			 <?php if(!is_page()){?>
                 <!-- AddThis Button BEGIN -->
                <style>
                    .at-icon-wrapper{
                        border-radius: 5px;
                    }
                </style>

                <span class="addthis_toolbox addthis_default_style addthis_32x32_style visible-xs">
                    <span class="share-label share-xs pull-left">Share </span> 
                    <a class="addthis_button_twitter"></a>
                    <a class="addthis_button_linkedin"></a>
                    <a class="addthis_button_email"></a>
                    <div class="clearfix"></div>
                </span>
                <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-529db6ac33c2ee8f"></script>
                <!-- AddThis Button END -->
                <div class="category-date">
                    <?php echo get_the_date("j. M Y"); ?>
                </div>

                <div class="category-date-devider"></div><br/>
			
			<?php } ?>
			<div class="clearfix"></div>
		</header><!-- .entry-header -->
	<?php }?>
	
	<div class="entry-content">
		<?php the_content(); ?> 
		<div class="clearfix"></div>
		
		<?php 
		
			$associated_members = get_post_meta( $post->ID, 'associated_members', true );
			
			if($associated_members){
				echo '<strong class="associated-members-title">';
				_e('Associated members:', 'bootstrap-basic');
				echo '</strong><br/><div class="associated-members">';
				$membersNum = count($associated_members)-1;
				foreach($associated_members as $key => $memberId){
					echo ' <a href="' . get_permalink($memberId) . '" class="associated-members paddingL5">'. get_the_title($memberId).'</a>';
					if($membersNum != $key){
						//echo ',';
						
					}
					
				}
				echo '</div>';
			}
			
		?>


		<div class="clearfix"></div>
		<?php
		/**
		 * This wp_link_pages option adapt to use bootstrap pagination style.
		 * The other part of this pager is in inc/template-tags.php function name bootstrapBasicLinkPagesLink() which is called by wp_link_pages_link filter.
		 */
		wp_link_pages(array(
			'before' => '<div class="page-links">' . __('Pages:', 'bootstrap-basic') . ' <ul class="pagination">',
			'after'  => '</ul></div>',
			'separator' => ''
		));
		?>
	</div><!-- .entry-content -->
	
	<footer class="entry-meta">
		<?php bootstrapBasicEditPostLink(); ?> 
	</footer>
</article><!-- #post-## -->