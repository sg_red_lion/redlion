<?php
/**
 * Template for displaying pages
 * 
 * @package bootstrap-basic
 */


$barristerId = (int) $_GET['id'];
$barristerObj = get_post($barristerId);


/*echo '<pre>';
var_export($barristerObj);
echo '</pre>';
*/

$silk = get_post_meta($barristerId, 'Silk', true);
$call = get_post_meta($barristerId, 'Call', true);
$phone = get_post_meta($barristerId, 'phone', true);
$email = get_post_meta($barristerId, 'email', true);
$certificate_one = get_post_meta($barristerId, 'certificate_one', true);
$certificate_two = get_post_meta($barristerId, 'certificate_two', true);
$honorary_door_tenant = get_post_meta($barristerId, 'honorary_door_tenant', true);
$area_of_experience = str_replace(array('[/expand]','[su_tabs]', '[/su_tab]' ,'[/su_tabs]'), ' ',wpautop( get_post_meta($barristerId, 'area_of_experience', true)));
$area_of_experience = str_replace(array('[expand title="','[su_tab title="'), ' <h3>', $area_of_experience);
$area_of_experience = str_replace('"]', ' </h3>', $area_of_experience);
$right_barrister_sidebar = get_post_meta($barristerId, 'right_barrister_sidebar', true);

?>
<HTML>
	<head>
	<?php wp_head();?>
	</head>
	<BODY>
		<br/><br/>
		<table style="width:100%">
			<tr>
				<td width="5%">
				</td>
				<td colspan="4">
					<img src="<?php echo get_template_directory_uri();?>/img/logo-header.png"/>
				</td>
			</tr>
			<tr>
				<td colspan="5"><br/></td>
			</tr>
			<tr>
				<td width="5%">
				</td>
				<td width="25%">
					<?php 
                    
                    
                    
                    if(get_the_post_thumbnail($barristerId, 'medium')){
                         echo get_the_post_thumbnail($barristerId, 'medium');
                    }else{
                        echo '<img src="' . get_template_directory_uri() . '/img/red pic.png"/>';
                    }
                    ?>
				</td>
				<td width="30%">
					<?php echo '<h1>'. $barristerObj->post_title. '</h1>';?><br/>
					
					<?php 
					
						echo 'Call:'.$call.'<br/>';
						if($silk){
							echo 'Silk:'.$silk.'<br/>';
						}
						if($phone){
							echo 'Phone:'.$phone.'<br/>';
						}
						if($email){
							echo 'Email:'.$email.'<br/>';
						}
						if($honorary_door_tenant == 'Yes'){
							echo 'Honorary Door Tenant<br/>';
						}
					?>
				
				
				</td>
				<td width="25%">
					<?php if($certificate_one){?>
						<img src="<?php echo get_field('certificate_one',$barristerId)['url']; ?>" width="100" height="130" />
					<?php }if($certificate_two){?>
						<img src="<?php echo get_field('certificate_two',$barristerId)['url']; ?>" width="100" height="130" />
					<?php }?>
				</td>
				<td width="5%">
				</td>
			</tr>
			<tr>
				<td width="5%"></td>
				<td colspan="3"><hr/></td>
				<td width="5%"></td>
			</tr>
			<tr>
				<td width="5%">
				</td>
				<td colspan="3">
					<?php echo apply_filters('the_content', $barristerObj->post_content);?><br/>
					<?php echo $area_of_experience; ?><br>
					<?php echo $right_barrister_sidebar; ?>
				</td>
				<td width="5%">
				</td>
			</tr>
		</table>
		<br/><br/>
	</BODY>
</HTML>
<?php wp_footer(); ?> 
