<?php
/**
 * Template for displaying single post (read full post page).
 * 
 * @package bootstrap-basic
 */

get_header();


?>
<div class="barttiersHeader" style="background: #F3F3F3 url(<?php  echo get_template_directory_uri().'/img/news-header-img@2x.png'; ?>); background-size: cover;">	

	
	<div class="imgCaption">
		<div class="col-xs-12 col-md-4 col-lg-3 padding0">
			<span class="title">
				<?php if(get_the_category( $id )){ 
				if(get_the_category( $id )[0]->slug == "news"){ 
					echo  "News";
				}else{
					echo  get_the_category( $id )[0]->name;
				}
					
				}
				
				?>			
				<div class="titleLine"></div>
			</span>
		</div>
		<div class="col-xs-7 col-md-8 col-lg-9 padding0 descriptionContainer">
			<span class="description">
			
			</span>	
		</div>
	</div><!--End .imgCaption-->
</div><!--End .barttiersHeader-->
<main id="main" class="site-main" role="main">
	<?php 		
		$category = get_the_category(); 
		$category_id = $category[0]->cat_ID;
	?>
	<div class="container-fluid singleBarristerHeader contentFontProperties" id="main-column">
		<div class="hidden-xs col-sm-12" id="category-search-area">
			<div class="form-group">
				<form method="post" action="<?php echo get_category_link( $category_id ); ?> ">			
					<?php
						
						$practiceAreaTerms = get_terms( array(
								'taxonomy' => 'practice-area',
								'hide_empty' => false,
							)
						);
							
						?>
					<select name="practice">
						<option value="" >Please Select</option>
						<?php 
                            $practice = (!empty($_POST['practice'])) ? $_POST['practice'] : '';
							foreach ( $practiceAreaTerms as $term ) {
								$selected = ($practice == $term->term_id) ? 'selected' : '';
								echo '<option value="' . $term->term_id . '" '.$selected.' >'.$term->name .'</option>';
							}
						?>	
					</select>
					<div class="inner-addon left-addon inlineBlock">
					  <i class="glyphicon glyphicon-search"></i>
					  <input type="text"  id="searchInput2" placeholder="Search" name="searchString" title="Search for:">
					</div>
					<input type="submit" name="submit" class="btn archive-search-btn" value="Search">
					<div class="clearfix visible-xs"></div>
					<span class="share-xs">Share</span>
					<?php if(get_option('general_setting_twitter')){?>
					<a class="twitter-follow-button" href="<?php echo get_option('general_setting_twitter'); ?>" target="_blank">
						<img src="<?php  echo get_template_directory_uri();?>/img/twitter@2x.png" alt="follow-us-twitter">
					</a>
					<?php }
					
					if(get_option('general_setting_linkedin')){?>
					<a class="linkedin-follow-button" href="<?php echo get_option('general_setting_linkedin'); ?>" target="_blank">
						<img src="<?php  echo get_template_directory_uri();?>/img/linked@2x.png" alt="join-us-linkedin">
					</a>
					<?php }?>
				</form>
			</div><!--End .form-group-->
		</div><!--End #category-search-area-->
		<div class="col-sm-8 col-md-9" id="single-content">
			<div class="col-sm-12 backWhite padding0-xs">
				<main id="main" class="site-main" role="main">
					<br/>

					<?php 
					while (have_posts()) {
						the_post();

						get_template_part('content', 'page');

						echo "\n\n";
						
						// If comments are open or we have at least one comment, load up the comment template
						
						//var_dump(wp_get_comment_status($id));
						if (get_option('default_comment_status') != 'closed' and (comments_open() || '0' != get_comments_number())) {
							comments_template();
						}

						echo "\n\n";

					} //endwhile;
					?> 
					<br/>
				</main>
			</div>
		</div>
		<div class="hidden-xs col-sm-4 col-md-3" id="single-sidebar">
			<div class="col-sm-12 backWhite padding0">
				<div class="padding20">
					<h3 class="fontS20"><?php _e('Sign up for news and events', 'bootstrap-basic'); ?> </h3>
					<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?><br/>
				</div>	
			</div>	
			
			<div class="clearfix"></div><br/>	
		    <?php get_template_part('content', 'rlc-sidebar'); ?>
            <script>
                $(document).ready(function(){
                    $( ".category-listing-devider" ).last().addClass('padding10').removeClass('category-listing-devider');

                });
            </script>
			<div class="clearfix"></div><br/>	
		</div>
	</div>
</main>
<?php //get_sidebar('right'); ?> 
<?php get_footer(); ?> 
