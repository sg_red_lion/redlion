<!--start desktop section-->
<div class="col-sm-12 paddingTB15 hidden-xs">
    <div class="col-sm-12">
        <div class="search-listing-devider"></div>
    </div>
    <div class="col-sm-4">
		<div class="redLine col-sm-3 padding5"></div>
		<h2 class="staff-name"><?php the_title(); ?></h2>
		<span class="text-muted"><?php the_field('position'); ?></span>
		<div>
			<hr class="col-sm-2" />
		</div>
    </div>
    
    <div class="clearfix"></div>
    <div class="col-sm-4 imageHolder">
		<img class="img-responsive" src="<?php if(get_the_post_thumbnail_url()){the_post_thumbnail_url();}else{ echo get_template_directory_uri().'/img/red-pic@2x.png';};?>" alt="" />
	</div>
	
	<div class="col-sm-8">
        <?php the_content(); ?>
	    <?php if(get_field('phone')){ ?>
		<div class="staff-contact-holder paddingR20">
			<img class="alignnone size-full wp-image-344 padding5 staff-contact" src="<?php echo get_template_directory_uri(); ?>/img/call-icon@2x.png" alt="" data-toggle="popover" data-trigger="focus" tabindex="0" data-placement="bottom" data-content="<strong>Telephone:</strong><a href='tel:<?php the_field('phone'); ?>'> <?php the_field('phone'); ?></a>" />
		</div>
		<?php } ?>
		<?php if(get_field('direct_email') or get_field('team_email') or get_field('cjsm_email')){ ?>
		<div class="staff-contact-holder">
			<img class="alignnone size-full wp-image-345 padding5 staff-contact" src="<?php echo get_template_directory_uri(); ?>/img/email-icon@2x.png" alt="" data-toggle="popover" data-trigger="focus" tabindex="0" data-placement="bottom" data-content="
			<strong>Direct email:</strong> <a href='mailto:<?php the_field('direct_email'); ?>'><?php the_field('direct_email'); ?></a> <br>
			<strong>Team email</strong>: <a href='mailto:<?php the_field('team_email'); ?>'> <?php the_field('team_email'); ?> </a> <br>
			<strong>CJSM email</strong>: <a href='mailto:<?php the_field('cjsm_email'); ?>'><?php the_field('cjsm_email'); ?> </a>" />
		</div>
		<?php } ?>
		
	</div>
</div>
<!--end desktop  section-->

<!--start mobile section-->
<div class="col-xs-12 opened-profile-xs" id="collapse-btn-container-<?php the_ID();?>">
	<div class="col-xs-4 staff-thumbnail-container-xs">
		<div class="thumbnail-xs" style="background: #F3F3F3 url(<?php if(get_field('mobile_image')){the_field('mobile_image');}else{ if(get_the_post_thumbnail_url()) the_post_thumbnail_url(); else echo get_template_directory_uri().'/img/red-pic@2x.png';};?>); background-size: 100%; background-repeat: no-repeat; background-position: center;">
			
		</div>
	</div>
	<div class="col-xs-8">
		<div class="border-bottom">
			<div class="redLine col-xs-4"></div>
			<div class="clearfix"></div>
			<h2 class="staff-title-xs"><?php the_title(); ?></h2>
			<div class="open-staff-profile" onclick="toggleProfile(<?php the_ID();?>, 'open')">+</div>
			<span class="text-muted"><?php the_field('position'); ?></span>
		</div>
	</div>
</div>
<!--end mobile section-->
<!--<div class="clearfix"></div>-->
<!--start collapse mobile section-->
<div class="hidden-md hidden-lg" id="collapse-content-<?php the_ID();?>">
	<div class="clearfix"></div>
	<div class="imageHolder">								  
		<img class="staff-thumbnail-opened" src="<?php if(get_the_post_thumbnail_url()){the_post_thumbnail_url();}else{ echo get_template_directory_uri().'/img/red-pic@2x.png';};?>" alt="">
		<img src="<?php echo get_template_directory_uri(); ?>/img/minus-outline.png" class="thumbnail-close-btn" onclick="toggleProfile()"/>
		<div class="staff-info-xs visible-xs">
			<div class="staff-name-xs"><?php the_title(); ?></div>
			<?php the_field('position'); ?>
		</div>
	</div>
	<div class="padding10"></div>
	<div class="col-xs-12">
		<?php the_content(); ?>
		<div class="clearfix"></div>
	</div>
	<div class="staff-contact-holder borderLeft">
		<img class="alignnone size-full wp-image-344 padding5 staff-contact" src="<?php echo get_template_directory_uri();?>/img/call-icon@2x.png" alt="" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="<strong>Telephone:</strong><a href='tel:<?php the_field('phone'); ?>'> <?php the_field('phone'); ?></a>" data-original-title="" title="">
		<div class="visible-xs h4 text-center">
			Call Profile
		</div>
	</div>
	<div class="staff-contact-holder borderRight borderLeft">
		<img class="alignnone size-full wp-image-345 padding5 staff-contact" src="<?php echo get_template_directory_uri();?>/img/email-icon@2x.png" alt="" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="
		<strong>Direct email:</strong> <a href='mailto:<?php the_field('direct_email'); ?>'><?php the_field('direct_email'); ?></a> <br>
			<strong>Team email</strong>: <a href='mailto:<?php the_field('team_email'); ?>'> <?php the_field('team_email'); ?> </a> <br>
			<strong>CJSM email</strong>: <a href='mailto:<?php the_field('cjsm_email'); ?>'><?php the_field('cjsm_email'); ?> </a>" />
		<div class="visible-xs h4 text-center">
			Email Profile
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="close-frofile-btn" onclick="toggleProfile()">
		Minimize
		<span class="pull-right">-</span>
	</div>			
<!--	<div class="clearfix"></div>-->
</div><!--end collapse mobile section-->