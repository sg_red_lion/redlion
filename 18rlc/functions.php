<?php
/**
 * Bootstrap Basic theme
 * 
 * @package bootstrap-basic
 */


/**
 * Required WordPress variable.
 */
include 'settings.php';
if( is_admin()) new RedLionSettingsPage();

if (!isset($content_width)) {
    $content_width = 1170;
}


if (!function_exists('bootstrapBasicSetup')) {
    /**
     * Setup theme and register support wp features.
     */
    function bootstrapBasicSetup() 
    {
        /**
         * Make theme available for translation
         * Translations can be filed in the /languages/ directory
         * 
         * copy from underscores theme
         */
        load_theme_textdomain('bootstrap-basic', get_template_directory() . '/languages');

        // add theme support title-tag
        add_theme_support('title-tag');

        // add theme support post and comment automatic feed links
        add_theme_support('automatic-feed-links');

        // enable support for post thumbnail or feature image on posts and pages
        add_theme_support('post-thumbnails');

        // allow the use of html5 markup
        // @link https://codex.wordpress.org/Theme_Markup
        add_theme_support('html5', array('caption', 'comment-form', 'comment-list', 'gallery', 'search-form'));

        // add support menu
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'bootstrap-basic'),
        ));

        // add post formats support
        add_theme_support('post-formats', array('aside', 'image', 'video', 'quote', 'link'));

        // add support custom background
        add_theme_support(
            'custom-background', 
            apply_filters(
                'bootstrap_basic_custom_background_args', 
                array(
                    'default-color' => 'ffffff', 
                    'default-image' => ''
                )
            )
        );
    }// bootstrapBasicSetup
}
add_action('after_setup_theme', 'bootstrapBasicSetup');


if (!function_exists('bootstrapBasicWidgetsInit')) {
    /**
     * Register widget areas
     */
    function bootstrapBasicWidgetsInit() 
    {
        register_sidebar(array(
            'name' => __('Sidebar right', 'bootstrap-basic'),
            'id' => 'sidebar-right',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));

        register_sidebar(array(
            'name' => __('Sidebar left', 'bootstrap-basic'),
            'id' => 'sidebar-left',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));

        register_sidebar(array(
            'name' => __('Header right', 'bootstrap-basic'),
            'id' => 'header-right',
            'description' => __('Header widget area on the right side next to site title.', 'bootstrap-basic'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));

        register_sidebar(array(
            'name' => __('Navigation bar right', 'bootstrap-basic'),
            'id' => 'navbar-right',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        ));

        register_sidebar(array(
            'name' => __('Footer left', 'bootstrap-basic'),
            'id' => 'footer-left',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));

        register_sidebar(array(
            'name' => __('Footer right', 'bootstrap-basic'),
            'id' => 'footer-right',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));
    }// bootstrapBasicWidgetsInit
}
add_action('widgets_init', 'bootstrapBasicWidgetsInit');


if (!function_exists('bootstrapBasicEnqueueScripts')) {
    /**
     * Enqueue scripts & styles
     */
    function bootstrapBasicEnqueueScripts() 
    {
        global $wp_scripts;

		
		
      
        wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.7');
        wp_enqueue_style('redLion-style', get_template_directory_uri() . '/css/redLion.css', array());
       // wp_enqueue_style('bootstrap-theme-style', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array(), '3.3.7');
        wp_enqueue_style('fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0');
       // wp_enqueue_style('main-style', get_template_directory_uri() . '/css/main.css');

        wp_enqueue_script('jquery-script', get_template_directory_uri() . '/js/vendor/jquery.min.js', array(), '3.1.1');
        wp_enqueue_script('modernizr-script', get_template_directory_uri() . '/js/vendor/modernizr.min.js', array(), '3.3.1');
        wp_register_script('respond-script', get_template_directory_uri() . '/js/vendor/respond.min.js', array(), '1.4.2');
        $wp_scripts->add_data('respond-script', 'conditional', 'lt IE 9');
        wp_enqueue_script('respond-script');
        wp_register_script('html5-shiv-script', get_template_directory_uri() . '/js/vendor/html5shiv.min.js', array(), '3.7.3');
        $wp_scripts->add_data('html5-shiv-script', 'conditional', 'lte IE 9');
        wp_enqueue_script('html5-shiv-script');
        wp_enqueue_script('jquery');
        wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', array(), '3.3.7');
        wp_enqueue_script('main-script', get_template_directory_uri() . '/js/main.js', array(), false, true);
        wp_enqueue_style('bootstrap-basic-style', get_stylesheet_uri());
		  
    }// bootstrapBasicEnqueueScripts
}
add_action('wp_enqueue_scripts', 'bootstrapBasicEnqueueScripts');


/**
 * admin page displaying help.
 */
if (is_admin()) {
    require get_template_directory() . '/inc/BootstrapBasicAdminHelp.php';
    $bbsc_adminhelp = new BootstrapBasicAdminHelp();
    add_action('admin_menu', array($bbsc_adminhelp, 'themeHelpMenu'));
    unset($bbsc_adminhelp);
}


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';


/**
 * Custom dropdown menu and navbar in walker class
 */
require get_template_directory() . '/inc/BootstrapBasicMyWalkerNavMenu.php';


/**
 * Template functions
 */
require get_template_directory() . '/inc/template-functions.php';


/**
 * --------------------------------------------------------------
 * Theme widget & widget hooks
 * --------------------------------------------------------------
 */
require get_template_directory() . '/inc/widgets/BootstrapBasicSearchWidget.php';
require get_template_directory() . '/inc/template-widgets-hook.php';

/* custom */

// Our custom post type function
function create_posttype() {

    register_post_type( 'barrister',
    // CPT Options
        array(
			'labels' => array(
				'name' => __( 'Barristers' ),
				'singular_name' => __( 'Barrister' )
			),
			'supports'  => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields',"post-formats"),
			'menu_position' => 4,
			'public' => true,
			'publicly_queryable' => true, 
			"menu_icon" => get_template_directory_uri() . "/img/icon_barristers.png",
			'has_archive' => true,
                'rewrite' => array('slug' => 'barrister'),
        )
    );
	
	register_post_type( 'our-seminars',
        array(
			'labels' => array(
				'name' => __( 'Seminars' ),
				'singular_name' => __( 'Seminars' )
			),
			'supports'  => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields',"post-formats"),
			'menu_position' => 4,
			'public' => true,
			'publicly_queryable' => true, 
			"menu_icon" => "dashicons-calendar",
			'has_archive' => true,
			'rewrite' => array('slug' => 'our-seminars'),
        )
    );
	
	register_post_type( 'history',
        array(
			'labels' => array(
				'name' => __( 'History' ),
				'singular_name' => __( 'history' )
			),
			'supports'  => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields',"post-formats"),
			'menu_position' => 4,
			'public' => true,
			'publicly_queryable' => true, 
			"menu_icon" => "dashicons-pressthis",
			'has_archive' => true,
			'rewrite' => array('slug' => 'history'),
        )
    );
	
	register_post_type( 'management',
        array(
			'labels' => array(
				'name' => __( 'Management and Staff' ),
				'singular_name' => __( 'management' )
			),
			'supports'  => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields',"post-formats"),
			'menu_position' => 4,
			'public' => true,
			'publicly_queryable' => true, 
			"menu_icon" => "dashicons-groups",
			'has_archive' => true,
			'rewrite' => array('slug' => 'management'),
        )
    );//flush_rewrite_rules( false );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );


function get_seminar_by_date(){
	
	$date_of_event_timestamp = strtotime($_POST['date']);
	$dateFormated =  date('d. M Y',$date_of_event_timestamp);
	$seminar_args = array(
			'post_type' => 'our-seminars',
			'post_status' => 'publish',
			'posts_per_page' => '1',
			'meta_query'       => array(
				'relation'    => 'AND',
				'date_of_event_clause' =>array(
					'key'          => 'date_of_event',
					'value'        => $dateFormated,
				)
		));
		$seminar = new WP_Query( $seminar_args );
	
		// The Loop
		if ( $seminar->have_posts() ) {
		
			while ( $seminar->have_posts() ) {
				$seminar->the_post();
				echo  get_permalink($seminar->ID);
				exit;
			}

		}
		/* Restore original Post Data */
		wp_reset_postdata();

}

add_action('wp_ajax_get_seminar_by_date', 'get_seminar_by_date');
add_action('wp_ajax_nopriv_get_seminar_by_date', 'get_seminar_by_date');

function rlc_start_session(){ 
	if( !session_id() ){
		session_start();
	}
}

function rlc_end_session() {
    session_destroy();
}

add_action('init', 'rlc_start_session', 1);
add_action('wp_logout', 'rlc_end_session');
add_action('wp_login', 'rlc_end_session');


function add_to_shortlist() {

	$barristerId = (int) $_POST['ID'];
	if(!in_array($barristerId, $_SESSION['barristers_shortlist'])){
		$_SESSION['barristers_shortlist'][] = $barristerId;
	}
	var_dump($_SESSION['barristers_shortlist']);
    wp_die(); 
}
add_action( 'wp_ajax_add_to_shortlist', 'add_to_shortlist');
add_action( 'wp_ajax_nopriv_add_to_shortlist', 'add_to_shortlist' );

function get_shortlist() {

	$barristers = array();
	foreach($_SESSION['barristers_shortlist'] as $barristerId){
		$barister = get_post((int)$barristerId);
			$barristers[] = array(
			'name'=> $barister->post_title, 
			'barister_id'=> $barister->ID, 
			'permalink' => get_permalink($barister->ID),
			'call' => get_post_field('Call',$barister->ID),
			'email' => get_post_field('email',$barister->ID),
			'cv' => get_post_field('cv',$barister->ID),
		);
		
	}
	echo json_encode($barristers);	
	wp_die();
}
add_action( 'wp_ajax_get_shortlist', 'get_shortlist');
add_action( 'wp_ajax_nopriv_get_shortlist', 'get_shortlist' );

function remove_from_shortlist() {

	$barristerId = (int) $_POST['ID'];
	if ($barristerId) {
		foreach ($_SESSION['barristers_shortlist'] as $key => $val) {
			if ($val == $barristerId) {
				unset($_SESSION['barristers_shortlist'][$key]);
			}
		}
	}else{
		unset($_SESSION['barristers_shortlist']);
	} 
	
    wp_die();
}
add_action( 'wp_ajax_remove_from_shortlist', 'remove_from_shortlist');
add_action( 'wp_ajax_nopriv_remove_from_shortlist', 'remove_from_shortlist' );

function count_shortlist() {
	echo count($_SESSION['barristers_shortlist']);
    wp_die();
}
add_action( 'wp_ajax_count_shortlist', 'count_shortlist');
add_action( 'wp_ajax_nopriv_count_shortlist', 'count_shortlist' );






/**/


// add hook
add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_sub_menu', 10, 2 );
// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
  if ( isset( $args->sub_menu ) ) {
    $root_id = 0;
    
    // find the current menu item
    foreach ( $sorted_menu_items as $menu_item ) {
      if ( $menu_item->current ) {
        // set the root id based on whether the current menu item has a parent or not
        $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
        break;
      }
    }
    
    // find the top level parent
    if ( ! isset( $args->direct_parent ) ) {
      $prev_root_id = $root_id;
      while ( $prev_root_id != 0 ) {
        foreach ( $sorted_menu_items as $menu_item ) {
          if ( $menu_item->ID == $prev_root_id ) {
            $prev_root_id = $menu_item->menu_item_parent;
            // don't set the root_id to 0 if we've reached the top of the menu
            if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
            break;
          } 
        }
      }
    }
    $menu_item_parents = array();
    foreach ( $sorted_menu_items as $key => $item ) {
      // init menu_item_parents
      if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;
      if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
        // part of sub-tree: keep!
        $menu_item_parents[] = $item->ID;
      } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
        // not part of sub-tree: away with it!
        unset( $sorted_menu_items[$key] );
      }
    }
    
    return $sorted_menu_items;
  } else {
    return $sorted_menu_items;
  }
}





function rlc_read_more($atts = [], $content = null, $tag = '') {

    static $i = 1;
    $i++;
    $text = '';
        $text .= '<div class="rlc-read-more" ><div data-toggle="collapse" data-target="#rlc_read_more_' . $i . '">See more <i class="caret"></i></div> </div>';
        $text .= '<div class="collapse" id="rlc_read_more_' . $i . '">';
            $text .= $content;
        $text .= '</div>';
    
    return  $text;
}
function register_shortcodes(){
  add_shortcode('rlc-read-more', 'rlc_read_more');
}
add_action( 'init', 'register_shortcodes');

