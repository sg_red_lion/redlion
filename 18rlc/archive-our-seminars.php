<?php
/**
*
* Displaying Our seminars page 
* @package bootstrap-basic
* Template Name: Our seminars page 
*
*/ get_header(); 

$seminars_args = array(
		'post_type' => 'our-seminars',
		'post_status' => 'publish',
		'posts_per_page' => '80',
	);
	$seminarsObj = new WP_Query( $seminars_args ); 
	$seminars = array();
	while($seminarsObj->have_posts()) { $seminarsObj->the_post();

		$date_of_event =  get_post_meta( get_the_ID(), 'date_of_event', true );							
		$date_of_event_timestamp = strtotime($date_of_event );
		$seminars[] = array(
			'date' => date('Y-m-d',$date_of_event_timestamp),
			"title"=>  get_permalink(),
			"classname"=> 'event-calendar',
		
		);
		
	}
?>
	<div class="barttiersHeader"  style="background: #F3F3F3 url(<?php echo get_template_directory_uri();?>/img/Group4@2x.png); background-size: cover;">
		<div class="imgCaption">
			<div class="col-xs-12 col-sm-4 padding0">
				<span class="title">
					<?php _e('Seminars', 'bootstrap-basic'); ?>
					<div class="titleLine"></div>
				</span>
			</div>
			<div class="col-xs-6 col-sm-8 padding0 descriptionContainer">
				<span class="description">
				
				</span>	
			</div>
		</div>
	</div>

	<div class="container-fluid singleBarristerHeader contentFontProperties" id="main-column">
		<div class="col-md-8 col-lg-9 padding0-xs">
			<div class="col-md-6 col-md-push-6" id="calendarContainer">
				<div class="col-sm-12 backWhite">
					<link rel="stylesheet" href="<?php echo  get_template_directory_uri(); ?>/css/zabuto_calendar.min.css">
					<script src="<?php echo  get_template_directory_uri(); ?>/js/vendor/zabuto_calendar.min.js"></script>
					
					<div id="my-calendar"></div>
					<div class="clearfix"></div>
					
					<script type="application/javascript">
					  $(document).ready(function () {
						var eventData = <?php echo json_encode($seminars) ?>;
						$("#my-calendar").zabuto_calendar({
						  cell_border: false,
						  data: eventData,
						 // today: true,
						  show_previous: 4,
						  action: function () {
								return myDateFunction(this,this.id);
						  },
						  
						  weekstartson: 0,
						  nav_icon: {
							prev: '<i class="fa fa-chevron-circle-left"></i>',
							next: '<i class="fa fa-chevron-circle-right"></i>'
						  }
						});
					  });
						function myDateFunction(obj, id) {
							
							var date = $("#" + id).data("date");
							var hasEvent = $("#" + id).data("hasEvent");
							
							
							if(hasEvent){
								 $.ajax({
									url: '<?php echo admin_url('admin-ajax.php'); ?>',    
									type: "POST",
									cache: false,
									data: 'date= '+date + '&action=get_seminar_by_date',
									dataType: 'html',
									success: function (a_href) {
								
										window.location.assign(a_href);
						
									}
								});

							}
							
							
							return false;
						}
					</script>
				</div>
				<div class="category-listing-devider"></div>
			</div>
			
			<div class="col-md-6 col-md-pull-6 padding0-xs">
				<div class="visible-xs">
					<div id="viewCalendar" onclick="viewCalendar()" class="btn btn-block">View Calendar</div>
					<script>
						function viewCalendar(){
							$('#calendarContainer').slideDown();
							$('#viewCalendar').slideUp();
						}
					</script><br/>
				</div>
				<div class="clearfix"></div>
			
				<div class="col-sm-12 backWhite" id="seminars-archive">
					<?php 
						/* Start the Loop */
						while (have_posts()) {
							the_post();

							/* Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 *
							 *get_template_part('content', get_post_format());
							 */
							
							  ?> 
							
								<h3>
									<a href="<?php echo get_permalink();?>">
										<?php echo get_the_title();?>
									</a>
								</h3>
							<div class="category-date">
								<?php the_field('date_of_event'); ?>
							</div>
							<div class="category-date-devider"></div>
							<div class="clearfix"></div><br/>
							<div class="category-excerpt">
								<?php the_excerpt();  ?>
							</div>
							
							<div class="clearfix"></div>
							<a href="<?php echo get_permalink();?>" class="btn btn-read-more"><?php _e('Read More', 'bootstrap-basic'); ?></a>
							<div class="category-listing-devider"></div>
						
					<?php } //endwhile; ?> 
					<?php bootstrapBasicPagination(); ?> 
				</div>
			</div>
		</div>
		<div class="hidden-xs hidden-sm col-md-4 col-lg-3" id="seminars-archive-sidebar">
			<div class="col-sm-12 backWhite padding0">
				<div class="padding20">
					<h3 class="fontS20"><?php _e('Sign up for news and events', 'bootstrap-basic'); ?> </h3>
					<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?><br/>
				</div>	
			</div>	
			
			<div class="clearfix"></div><br/>	
           
            <script>
                $(document).ready(function(){
                    $( ".category-listing-devider" ).last().addClass('padding10').removeClass('category-listing-devider');

                });
            </script>
		    <?php get_template_part('content', 'rlc-sidebar'); ?>
			<div class="clearfix"></div><br/>	
		</div><!--End #seminars-archive-sidebar-->
		<div class="clearfix"></div><br/>
	</div>
	<script>
		$(document).ready(function(){
			$( ".category-listing-devider" ).last().addClass('padding10').removeClass('category-listing-devider');
			//$('.badge-today').parent().css('background','#c61912').css('border-radius','50%');
		});
	</script>
<?php get_footer(); ?> 
