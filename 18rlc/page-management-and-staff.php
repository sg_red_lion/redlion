<?php
/**
 * Template for displaying page Management and Staff
 * 
 * @package bootstrap-basic
 
 
 
 
 */
get_header();

$parentId = wp_get_post_parent_id(get_the_ID()); 
	$staff_args = array(
		'post_type' => 'management',
		'post_status' => 'publish',
		'posts_per_page' => '1000',
		'orderby' => 'display_section',
		'order' => 'ASC',
		'meta_query'       => array(
			'relation'    => 'AND',
			'seniority_clause' =>array(
				'key'          => 'display_section',
			)			
		),
	); 


	 wp_enqueue_style('management-and-staff', get_template_directory_uri() . '/css/management_and_staff.css', array());
 ?> 
<script>
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover({html:true}); 
	});

	function toggleProfile(id, action){
		$('[id^="collapse-btn-container-"]').slideDown();
		$('[id^="collapse-content-"]').slideUp();
		
		if (action == 'open' && id ){
			$('#collapse-btn-container-' + id).slideUp();
			$('#collapse-content-'+ id).slideDown();
		}
	}				
</script>
<?php 
$header_image = get_template_directory_uri().'/img/Group.png';
if(get_the_post_thumbnail_url()){$header_image =  get_the_post_thumbnail_url();}
?>
<div class="barttiersHeader" style="background: #F3F3F3 url(<?php echo $header_image; ?>); background-size: cover;">		

	<div class="imgCaption">
		<div class="col-xs-12 col-sm-4  padding0">
			<span class="title">
				<?php echo get_the_title()?>		
				<div class="titleLine"></div>
			</span>
		</div>
		<div class="col-xs-4 col-sm-8 padding0  descriptionContainer">
			<span class="description"></span>	
		</div>
	</div>
</div>

<div class="container-fluid padding0 singleBarristerHeader contentFontProperties" id="main-column">
	<main id="main" class="site-main" role="main">
		<div class="col-sm-8  col-md-9" id="staff-content">
		<div class="backWhite">
            <div class="col-sm-12" id="management_top_description">
                <?php the_field('management_top_description')?>
            </div>
			<?php 
				$staff = new WP_Query( $staff_args ); 
				if(($staff->have_posts())) {
                    $printed = array();
					while ( $staff->have_posts() ) : $staff->the_post(); 
                    
                        $next_post = get_next_post();
                        if(get_field('display_section') !== '1' 
                           and !isset($printed['printed_'. get_field('display_section')])){

                            $printed['printed_'. get_field('display_section')] = 1 ;
                            $field = get_field_object('field_58c00ea0cca4c');
                            $value = get_field('display_section');
                            $label = $field['choices'][ $value ];
                            echo '<div class="clearfix visible-xs"></div>
                                    <div class="col-sm-12 marginT5">
                                        <div class="search-listing-devider"></div>
                                    </div>';
                            echo '<h2 class="display-section-title">' . $label .'</h2>';
                            

                        }
                        get_template_part('content', 'staff');
			
					endwhile; 
				}
				wp_reset_postdata(); 
			 ?>	
		
			<div class="col-sm-12 visible-lg">
                <div class="search-listing-devider"></div>
            </div>
			<div class="col-xs-12">	
			
				<?php the_content(); ?>
				<div class="clearfix"></div><br/>
				<a href="/our-people/barristers/" id="view-our-barrister-btn" class=' visible-xs'>Vew Our Barristers</a>
				<strong><u><a href="/our-people/barristers/" class="hidden-xs">Click here to view our Barristers</a></u></strong>
				<div class="padding10"></div>
			</div>
			<div class="clearfix"></div><br/>
		</div>
		</div>
		<div class="hidden-xs col-sm-4 col-md-3" id="staff-sidebar">
			<?php 
			 $args = array(
				'post_type'      => 'page',
				'posts_per_page' => -1,
				'post_parent'    => $parentId,
				'order'          => 'ASC',
				'orderby'        => 'menu_order'
			 );
			 
			if($parentId){
				$parent = new WP_Query( $args );
		
				
				if ( $parent->have_posts() ) : ?>
					<div class="col-sm-12 backWhite"> 
                        <h3 class="fontS20 ">
                            <?php /*<a href="<?php the_permalink($parentId); ?>" title="<?php the_title($parentId); ?>">
                            <?php echo get_the_title($parentId).'<br/>';?></a>
                            */?>
                            <?php _e('In this section', 'bootstrap-basic'); ?>
                        </h3>
                        <ul id="parentPagesList">
                            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                                <li>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </li>

                            <?php endwhile; ?>
                        </ul>
					</div>
					<div class="clearfix"></div><br/>
				<?php endif; wp_reset_query(); 	
			
			} ?>

		
			<?php get_template_part('content', 'rlc-sidebar'); ?>
		</div>
	</main>
</div>

<?php get_footer(); ?> 