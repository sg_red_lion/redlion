<?php 
/**
*
* Displaying Single Our-Seminar page 
* @package bootstrap-basic
* 
*
*/ 
get_header(); 
?>
<div class="barttiersHeader" style="background: #F3F3F3 url(<?php echo get_template_directory_uri();?>/img/Group4@2x.png); background-size: cover;">	
	<div class="imgCaption">
		<div class="col-xs-12 col-sm-4 padding0">
			<span class="title">
				<?php _e('Seminars', 'bootstrap-basic'); ?>
				<div class="titleLine"></div>
			</span>
		</div>
		<div class="col-xs-6 col-sm-8 padding0 descriptionContainer">
			<span class="description">
			
			</span>	
		</div>
	</div>
</div>


<div class="container-fluid padding0 singleBarristerHeader contentFontProperties" id="main-column">
	<main id="main" class="site-main" role="main">
		<div class="col-md-8 col-lg-9" id="single-seminar-content">
			<div class="col-sm-12 backWhite padding0-xs">
				<div class="col-sm-12 padding0">
					<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						}
					?>
				
					<h1><?php the_title(); ?></h1>
					<div class="category-date">
						<?php the_field('date_of_event'); ?>
						<?php //echo get_the_date("j. M Y"); ?>
					</div>
					<div class="category-date-devider"></div>
					<div class="clearfix"></div>
					<?php the_content(); ?>
				</div>
				<div class="col-sm-12 padding0">
					<?php 
					
						$associated_members = get_post_meta( $post->ID, 'associated_members', true );
						
						if($associated_members){
							echo '<strong>';
							_e('Associated members:', 'bootstrap-basic');
							echo '</strong><br/>';
							$membersNum = count($associated_members) -1;
							foreach($associated_members as $key => $memberId){
								echo '<a href="' . get_permalink($memberId) . '" class="associated-members paddingR5">'. get_the_title($memberId).'</a>';
								if($membersNum != $key){
									//echo ', ';
									
								}
								
							}
						}
						
					?>
				</div>
				<div class="clearfix"></div><br/>
				<div class="col-sm-12 padding0">
					<button id="viewRegisterFormBtn" class="btn btn-register-interest" onclick="viewRegisterForm()">
						<?php _e('Register Interest', 'bootstrap-basic'); ?>
					</button>
				</div>
					<div class="clearfix"></div><br/>
					<script>
						
						$( document ).ready(function() {
							
							if ($('.nf-form-cont').length){
								$('#viewRegisterFormBtn').css('display' , 'block');
								$('.nf-form-cont').slideUp();
							}else{
								$('#viewRegisterFormBtn').addClass('hidden');
							}
						});
						function viewRegisterForm(){
							$('.nf-form-cont').slideDown();
							$('#viewRegisterFormBtn').removeClass('visible-xs').addClass('hidden');
							
						}
					</script>
			</div>
		</div>
		<div class="hidden-xs hidden-sm col-md-4 col-lg-3" id="single-seminar-sidebar">
			<div class="col-sm-12 backWhite padding0">
                <h3 class="fontS20 paddingL20"><?php _e('Sign up for news and events', 'bootstrap-basic'); ?> </h3>
				<div class="padding20">
					<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?><br/>
				</div>	
			</div>	
			
			<div class="clearfix"></div><br/>	
			<?php get_template_part('content', 'rlc-sidebar'); ?>
            <script>
                $(document).ready(function(){
                    $( ".category-listing-devider" ).last().addClass('padding10').removeClass('category-listing-devider');

                });
            </script>
            <div class="clearfix"></div><br/>	
		</div><!--End #single-seminar-sidebar-->
	</main>
</div><!--End #main-column-->

<?php get_footer(); ?> 