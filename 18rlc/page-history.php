<?php
/**
 * Template for displaying pages
 * 
 * @package bootstrap-basic
 */
get_header();

$history_args = array(
		'post_type' => 'history',
		'post_status' => 'publish',
		'order' => 'ASC',
		'posts_per_page' => '1000',
	);
 $history_posts = new WP_Query( $history_args ); 
 ?> 
<style>
.timeline-year-left{
    margin-left: 30px;
    color: white;
    font-size: 30px;
    margin-top: 15px;
    position: absolute;
    width: 100px;
    text-align: center;
}
.timeline-year-right{

    color: white;
    font-size: 30px;
    margin-top: 15px;
    position: absolute;
	margin-left: -130px;
    width: 100px;
    text-align: center;
}

.singleBarristerHeader + div + div{
	    background-color: white;
}
.vertical-devider{
    background: #c61912;
    text-align: center;
    overflow: hidden;
    font-size: 35px;
    color: #fff;
    width: 100px;
    height: 100%;
    padding-bottom: 1425px;
    margin-bottom: -1320px;
}
.history-left-side{
	text-align:right;
	float:left;
	padding-right: 30px;
	  
}
.history-right-side{
	text-align:left;
	float: right;
	padding-left: 30px;
}
#vertical-devider{
	background: #c61912;
    text-align: center;
    overflow: hidden;
    font-size: 35px;
    color: #fff;
    width: 100px;
	display:inline-block;
    float:none;
	position: absolute;
    text-align:left;
    margin-right:-4px;
}
.timeLineRedSide:last{
	
}
</style>

<div class="contentFontProperties" id="main-column">
	<main id="main" class="site-main" role="main">
	
		<div class="col-sm-12 backWhite padding0" id="timeline-container">	
			<div id="vertical-devider" class="hidden-xs"></div>	
			<?php 
			
			if(($history_posts->have_posts())) {
				$counter = 0;
				while ( $history_posts->have_posts() ) : $history_posts->the_post(); 
			
			?>	
			<div class="hidden-lg">
				<div class="yearContainer">
					<div class="col-xs-2 timeLineYear">
						<span><?php the_field('year'); ?></span>
					</div>
					<?php the_post_thumbnail('large', array('class' => 'img-responsive'));  ?>
				</div>
				
				<div class="yearContainer">
					<div class="col-xs-2 timeLineRedSide">
						
					</div>
					<div class="col-xs-10 timeLineContent">	
						<h2><?php the_title();?></h2>
						<div class="history-timeline-devider"></div>
						
						<?php the_content(); ?>
					</div>
				
				</div>
			</div>

			<div class="visible-lg">
			<?php 
				$isOdd = false;
				
				if($counter % 2 != 0){
					$isOdd = true;
				}
			$counter++;
				if(!$isOdd){ 
				?>	
					
					<div class="history-left-side" id="side-<?php the_ID()?>">
						<?php if($history_posts->post_count != $counter){ ?>
							<span class="timeline-year-left"><?php the_field('year'); ?></span>
							<h2><?php the_title();?></h2>
							<div class="history-timeline-devider pull-right"></div>
						<?php } ?>
						
						<?php the_post_thumbnail('full', array('class' => 'img-responsive'));  ?>
						<?php if($history_posts->post_count == $counter){ ?>
							<span class="timeline-year-left"><?php the_field('year'); ?></span>
							<h2><?php the_title();?></h2>
							<div class="history-timeline-devider pull-right"></div>
						<?php } ?>
						
						<div class="clearfix"></div>
						<div class="paddingL20 <?php if($history_posts->post_count != $counter){echo 'marginT20'; }?>">
							<?php the_content();?>
						</div>
					</div>
				<?php  }else{ ?>	
					<div class="history-right-side">
						<div class="marginT100"></div>
						<?php the_post_thumbnail('full', array('class' => 'img-responsive'));  ?>
						<span class="timeline-year-right"><?php the_field('year'); ?></span>
						<h2><?php the_title();?></h2>
						<div class="history-timeline-devider pull-left"></div>
						<div class="clearfix"></div>
						<div class="paddingR20">
							<?php the_content(); ?>
						</div>
					</div>
				<?php } ?>

			</div>
			
			<?php 
				
			endwhile; 
			}

			wp_reset_postdata();

			?>
			<div class="clearfix"></div>
		</div>

	</main>
</div>

<script>
function resizeColumns(){	

	var windowWidth = $( window ).width();
	var half = windowWidth / 2 - 50;
	$('.history-right-side, .history-left-side').css('width',half);
	var timelineContainerHeight = $('#timeline-container').height();//-30
	
	$('#vertical-devider').height(timelineContainerHeight).css('margin-left',half);
	if(windowWidth < 1200){
		$('#vertical-devider').addClass('hidden');
	}else{
		$('#vertical-devider').removeClass('hidden');
	}
}   
setTimeout(resizeColumns, 400);

 $(window).resize(resizeColumns);

</script>
<?php get_footer(); ?> 